﻿using Bid.Business.Hubs;
using Bid.Business.Interfaces;
using Bid.Domain;
using Bid.Domain.Models;
using Microsoft.AspNetCore.SignalR;

namespace Bid.Business.Services
{
    public class HubsService : IHubsService
    {
        private readonly IHubContext<CommunicationUiHub> _generalHub;
        
        private readonly IHubContext<BidCommunicationUiHub> _bidHub;

        private readonly IEmailMessageSenderService _emailMessageSender;
        public HubsService(IHubContext<CommunicationUiHub> generalHub, IHubContext<BidCommunicationUiHub> bidHub, IEmailMessageSenderService emailMessageSender)
        {
            _generalHub = generalHub;
            _bidHub = bidHub;
            _emailMessageSender = emailMessageSender;
        }
        public Task OnSendAuctionEnded(string  itemName, string itemPrice)
        {
            return _generalHub.Clients.All.SendAsync(HubEvents.OnAuctionEnded, itemName,itemPrice);
        }
        public Task OnSendNewItem(ItemBase model)
        {
            return _generalHub.Clients.All.SendAsync(HubEvents.OnReceivedNewItem, model);
        }
        public Task OnSendUpdatedItem(ItemBase model, Guid userId)
        {
            _ = _generalHub.Clients.All.SendAsync(HubEvents.OnUpdatedItem, model);
            _ = _bidHub.Clients.All.SendAsync(HubEvents.OnUpdatedItem, model, userId);
            return Task.CompletedTask;
        }
        public Task OnSendDeletedItem(Guid id)
        {
            return _generalHub.Clients.All.SendAsync(HubEvents.OnDeletedItem, id);
        }
    }
}