﻿using Bid.Business.Interfaces;
using Bid.DataAccess.Interfaces;
using Bid.Domain.Models;

namespace Bid.Business.Services;

public class UserSettingsService:IUserSettingsService
{
    private readonly IUserSettingsDataController _userSettingDataController;
    
    public UserSettingsService(IUserSettingsDataController userSettingDataController)
    {
        _userSettingDataController = userSettingDataController;
    }
    public async Task<bool> InsertOrUpdate(UserSettingsModel model)
    {
        var res = await _userSettingDataController.InsertOrUpdate(model);
        var res1 = await _userSettingDataController.UpdateAvailableBalanceByNewSettings(model.UserId);
        return res & res1;
    }
    public async Task<bool> UpdateAvailableBalanceByUserIdAndAuctionId(Guid userId, Guid auctionId)
    {
        var res= await _userSettingDataController.UpdateAvailableBalanceByUserIdAndAuctionId(userId,auctionId);
        return res;
    }
    public async Task<bool> UpdateAvailableBidCountByItemId(Guid itemId)
    {
        return await _userSettingDataController.UpdateAvailableBalanceByItemId(itemId);
    }

    public async Task<UserSettingsModel> GetSettingsByUserId(Guid userId)
    {
        return await _userSettingDataController.GetSettingsByUserId(userId);
    }
}