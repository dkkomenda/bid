﻿using Bid.Business.Interfaces;
using Bid.Domain.Helpers;
using Microsoft.Extensions.Logging;


namespace Bid.Business.Services
{
    internal class ImageService : IImageService
    {
        private readonly ILogger<ImageService> _logger;

        public ImageService(ILogger<ImageService> logger)
        {
            _logger = logger;
        }

        public Task SaveImage(Guid id,string sectionName, byte[] data, int width, int height)
        {
            var resizedDate = ImageHelper.ResizeImage(data, width, height);

            if (resizedDate == null)
                return Task.CompletedTask;
            
            var path = CreateSection(sectionName);
            
            return Save(Path.Combine(path, $"{id.ToString()}.png"), data);
        }
        public Task SaveUserImage(Guid userId, byte[] data)
        {
            if (data == null)
                return Task.FromResult<byte[]>(null);
            var path = CreateSection("UserImages");
            return Save(Path.Combine(path, $"{userId.ToString()}.png"), data);
        }
        public Task<byte[]> GetImage(Guid id, string sectionName)
        {
            try
            {  
                var path = Path.Combine(GetTempFolder(), sectionName, $"{id.ToString()}.png");
                return File.Exists(path) ? File.ReadAllBytesAsync(path) : Task.FromResult<byte[]>(null);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return Task.FromResult<byte[]>(null);
            }
        }
        public Task<string> GetImagePath(Guid id, string sectionName)
        {
            try
            {  
                var path = Path.Combine(GetTempFolder(), sectionName, $"{id.ToString()}.png");

                return Task.FromResult(path);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null;
            }
        }
        public Task<byte[]> GetUserImage(Guid userId,  bool isSmall)
        {
            try
            {
                var path = Path.Combine(GetTempFolder(), "UserImages", $"{userId.ToString()}.png");
                if (File.Exists(path))
                    return File.ReadAllBytesAsync(path);
                return Task.FromResult<byte[]>(null);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return Task.FromResult<byte[]>(null);
            }
        }
        private async Task Save(string path, byte[] data)
        {
            try
            {
                if (File.Exists(path)) 
                    File.Delete(path);
                _logger.LogInformation($"Save image to path {path}");
                using (var stream = File.Create(path)) 
                    await stream.WriteAsync(data);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }
        public string CreateSection(string name)
        {
            var path = Path.Combine(GetTempFolder(), name);
            if (!Directory.Exists(path)) 
                Directory.CreateDirectory(path);
            return path;
        }
        private string GetTempFolder()
        {
            var tempFolder = Path.Combine(AppContext.BaseDirectory, "temp");
            if (!Directory.Exists(tempFolder)) 
                Directory.CreateDirectory(tempFolder);
            return tempFolder;
        }
    }
}