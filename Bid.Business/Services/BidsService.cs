﻿using Bid.Business.Interfaces;
using Bid.DataAccess.Interfaces;
using Bid.Domain.Enums;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;

namespace Bid.Business.Services;

public class BidsService : IBidsService
{
    private readonly IBidsDataController _bidsDataController;
    private readonly IItemsService _itemsService;
    private readonly IUserSettingsService _userSettingsService;


    public BidsService(IBidsDataController bidsDataController, IItemsService itemsService,IUserSettingsService userSettingsService)
    {
        _bidsDataController = bidsDataController;
        _itemsService = itemsService;
        _userSettingsService = userSettingsService;
    }

    public async Task<bool> InsertOrUpdate(BidModel model)
    {
        var itemId = await _bidsDataController.GetItemIdByAuctionId(model.AuctionId);
        var itemUpdated = await _itemsService.UpdateItemPriceById(itemId, model.Bid, model.UserId);
        var bidInserted = await _bidsDataController.InsertOrUpdate(model);
        var bidsUpdated =  await _bidsDataController.ChangeBidsStatusByAuctionId(model.AuctionId);
        var userAvailableBidUpdated = await _userSettingsService.UpdateAvailableBalanceByUserIdAndAuctionId(model.UserId,model.AuctionId);
        return itemUpdated && bidInserted && bidsUpdated && userAvailableBidUpdated;
    }

    public async Task<IEnumerable<AuctionBidDetailModel>> GetByAuctionId(Guid auctionId)
    {
        return await _bidsDataController.GetByAuctionId(auctionId);
    }
    public async Task<int> GetLastBidByAuctionId(Guid auctionId)
    {
        return await _bidsDataController.GetLastBidByAuctionId(auctionId);
    }
    public async Task<IEnumerable<BidViewModel>> GetBidViewModelsByUserId(Guid userId)
    {
        return await _bidsDataController.GetBidViewModelsByUserId(userId);
    }

    public async Task<bool> SetWonBidByAuctionId(Guid auctionId)
    {
        return await _bidsDataController.SetWonBidByAuctionId(auctionId);
    }
}