﻿using System.Net;
using System.Net.Mail;
using Bid.Business.Interfaces;
using Bid.Domain.Enums;
using Bid.Domain.Helpers;
using Bid.Domain.Models;

namespace Bid.Business.Services;

public class EmailMessageSenderService:IEmailMessageSenderService
{
    private bool _isSuccessSend;

    private string _sendFromEmail;
    private string _sendFromEmailPassword;
    private string _hostSmtp;
    private int _hostPort;
    private bool _isHtmlBody;

    public EmailMessageSenderService()
    {
        _isHtmlBody = false;
        _hostPort = 587;
        _hostSmtp = "smtp.gmail.com";
        _sendFromEmailPassword = "869Q}xy#,3t`tH]y";
        _sendFromEmail = "notifications@mint-innovations.com";
    }

    public async Task<ResponseMessageProviderEnum> SendMessage(string messageText, string messageTitle, string[] targets)
    {
        try
        {
            _isSuccessSend = true;

            if (string.IsNullOrEmpty(_sendFromEmail) || string.IsNullOrEmpty(_sendFromEmailPassword))
                return ResponseMessageProviderEnum.ValidationError;

            if (string.IsNullOrEmpty(messageText) || targets == null || targets.Length == 0)
                return ResponseMessageProviderEnum.ValidationError;

            var message = new MailMessage();
            message.From = new MailAddress(_sendFromEmail);
            message.Body = messageText;
            message.Subject = messageTitle;
            message.IsBodyHtml = _isHtmlBody;

            for (var i = 0; i < targets.Length; i++)
            {
                message.To.Add(targets[i]);
            }

            var smtpClient = new SmtpClient(_hostSmtp, _hostPort)
            {
                UseDefaultCredentials = false,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            smtpClient.Credentials = new NetworkCredential(_sendFromEmail, _sendFromEmailPassword);
            Console.WriteLine($"Send to email: {targets[0]}");
            smtpClient.SendCompleted += (s, e) =>
            {
                if (e.Error == null)
                    _isSuccessSend = true;
                else
                {
                    Console.WriteLine($"Error send message: {e.Error.Message}");
                    _isSuccessSend = false;
                }

                smtpClient.Dispose();
                message.Dispose();
            };

            await smtpClient.SendMailAsync(message);

            return _isSuccessSend ? ResponseMessageProviderEnum.Success : ResponseMessageProviderEnum.ProviderError;

        }
        catch (Exception ex)
        {
            return ResponseMessageProviderEnum.ProviderError;
        }
    }

    public async Task<bool> PrepareMessage(WinnerLetterModel letterModel)
    {
        var messageTitle = "Congratulations";
        var messageText = $"{letterModel.FirstName} {letterModel.LastName}, you won {letterModel.ItemName}. Price - {letterModel.ItemPrice}";
        string[] targets = new[] { letterModel.Email };
        var res = await SendMessage(messageText, messageTitle, targets);
        return _isSuccessSend ? res == ResponseMessageProviderEnum.Success : res == ResponseMessageProviderEnum.ProviderError;
    }
}