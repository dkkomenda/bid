﻿using Bid.Business.Interfaces;
using Bid.DataAccess.Interfaces;
using Bid.Domain.Models;

namespace Bid.Business.Services;

public class ItemsService : IItemsService
{
    private readonly IItemsDataController _itemsDataController;
    private readonly IImageService _imageService;
    private readonly IHubsService _hubService;
    private readonly IUserSettingsService _userSettingsService;
    
    public ItemsService(IItemsDataController itemsDataController,
        IImageService imageService, 
        IHubsService hubService, IUserSettingsService userSettingsService)
    {
        _itemsDataController = itemsDataController;
        _imageService = imageService;
        _hubService = hubService;
        _userSettingsService = userSettingsService;
    }
    public async Task<bool> Create(ItemBase item)
    {
        item.Image = await _imageService.GetImagePath(item.Id, "ItemsLogo");
        var res = await _itemsDataController.Create(item);
        if (res)
        {
            await _hubService.OnSendNewItem(item);
            return true;
        }
        return res;
    }
    public async Task<bool> Update(ItemBase item)
    {
        item.Image = await _imageService.GetImagePath(item.Id, "ItemsLogo");
        var res = await _itemsDataController.UpdateItem(item);
        if (res)
        {
            await _hubService.OnSendUpdatedItem(item, Guid.Empty);
            return true;
        }
        return res;
    }
    public async Task<bool> Delete(Guid itemId)
    {
        _ = await _userSettingsService.UpdateAvailableBidCountByItemId(itemId);
        var res = await _itemsDataController.DeleteItem(itemId);
        if (res)
        {
            await _hubService.OnSendDeletedItem(itemId);
            return true;
        }
        return res;
    }
    public async Task<bool> UpdateItemPriceById(Guid itemId, int price, Guid userId)
    {
        var res = await _itemsDataController.UpdateItemPriceById(itemId,price);
        var item = await _itemsDataController.GetItemById(itemId);
        await _hubService.OnSendUpdatedItem(item,userId);
        
        return res;
    }
    public async Task<IEnumerable<ItemBase>> GetAllItems()
    {
        return await _itemsDataController.GetAllItems();
    }
    public async Task<ItemBase> GetItemById(Guid itemId)
    {
        return await _itemsDataController.GetItemById(itemId);
    }

    public async Task<byte[]> GetImageByItemId(Guid id)
    {
        var path = await _itemsDataController.GetImageById(id);
        if (File.Exists(path))
            return await File.ReadAllBytesAsync(path);
            
        return null;
    }
}