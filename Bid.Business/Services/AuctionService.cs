﻿using Bid.Business.Interfaces;
using Bid.DataAccess.Interfaces;
using Bid.Domain.Models;

namespace Bid.Business.Services;

public class AuctionService: IAuctionsService
{
    private readonly IAuctionsDataController _auctionsDataController;
    
    private readonly IBidsService _bidsService;
    
    private readonly IEmailMessageSenderService _emailMessageSender;
    
    private readonly IHubsService _hubService;
    
    private System.Timers.Timer _timer = new();
    
    private List<AuctionModel> _auctionTimerList = new();

    
    public AuctionService(IAuctionsDataController auctionsDataController,
        IHubsService hubService,
        IBidsService bidsService,
        IEmailMessageSenderService emailMessageSender)
    {
        _auctionsDataController = auctionsDataController;
        _hubService = hubService;
        _bidsService = bidsService;
        _emailMessageSender = emailMessageSender;
        RefreshAuctionList();
    }
    private void RefreshAuctionList()
    {
        _auctionTimerList = GetActiveAuctions().Result.ToList();
        StartTimer();
    }
    private void StartTimer()
    {
        var nearestAuction = _auctionTimerList.OrderBy(x => x.EndedAt).FirstOrDefault();
        if (nearestAuction != null)
        {
            var time = nearestAuction.EndedAt.Subtract(DateTime.Now).TotalMilliseconds;
            if (time <= 0)
            {
                Stop(nearestAuction);
                return;
            }

            _timer = new (time);
            _timer.Elapsed += (_,_) => Stop(nearestAuction);
            _timer.Start();
        }
    }
    private void Stop(AuctionModel auction)
    {
       _timer.Stop();
       _timer.Dispose();
       auction.IsActive = false;
       _ = ChangeAuctionStatus(auction).Result;
       SendLetterToWinner(auction);
       RefreshAuctionList();
    }
    private void SendLetterToWinner(AuctionModel auction)
    {
        WinnerLetterModel letter =  GetWinnerLetter(auction).Result;
        if(letter == null)
            return;
        
        _ = _emailMessageSender.PrepareMessage(letter).Result;
        _ =  _hubService.OnSendAuctionEnded(letter.ItemName, letter.ItemPrice);
    }
    private async Task<WinnerLetterModel> GetWinnerLetter(AuctionModel auction)
    {
        return await _auctionsDataController.GetWinnerLetter(auction);
    }
    public async Task<bool> InsertOrUpdate(AuctionModel model)
    {
        var res = await _auctionsDataController.InsertOrUpdate(model);
        if (!res)
            return false;

        RefreshAuctionList();
        return res;
    }

    public async Task<AuctionModel> GetById(Guid modelId)
    {
        return await _auctionsDataController.GetById(modelId);
    }
    public async Task<bool> DeleteByItemId(Guid itemId)
    {
        return await _auctionsDataController.DeleteByItemId(itemId);
    }
    public async Task<bool> ChangeAuctionStatus(AuctionModel auction)
    {
        var res1 = await _auctionsDataController.ChangeAuctionStatus(auction.Id, auction.IsActive);
        var res2 = await _bidsService.SetWonBidByAuctionId(auction.Id);
        return res1 && res2;
    }
    public async Task<AuctionModel> GetByItemId(Guid itemId)
    {
        return await _auctionsDataController.GetByItemId(itemId);
    }
    public async Task<IEnumerable<AuctionModel>> GetActiveAuctions()
    {
        return await _auctionsDataController.GetActiveAuctions();
    }
}