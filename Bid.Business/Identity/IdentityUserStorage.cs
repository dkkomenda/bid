using System.Security;
using System.Security.Claims;
using Bid.DataAccess.DataControllers;
using Bid.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Bid.Business.Identity
{
    public class IdentityUserStorage: IUserPasswordStore<AppIdentityUser>,
        IUserSecurityStampStore<AppIdentityUser>,
        IUserEmailStore<AppIdentityUser>,
        IUserLockoutStore<AppIdentityUser>,
        IUserRoleStore<AppIdentityUser>,
        IUserClaimStore<AppIdentityUser> 
    {
        private readonly UsersDataController _userDataController;
        private readonly RolesDataController _roleDataController;
        private readonly ILogger<IdentityUserStorage> _logger;

        public IdentityUserStorage(
            UsersDataController userDataController, 
            RolesDataController roleDataController,
            ILogger<IdentityUserStorage> logger)
        {
            _userDataController = userDataController;
            _roleDataController = roleDataController;
            _logger = logger;
        }
        public void Dispose()
        {
        }

        public Task<string> GetUserIdAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.Id.ToString());
        }

        public Task<string> GetUserNameAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.UserName);
        }

        public Task SetUserNameAsync(AppIdentityUser user, string userName, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }

            user.UserName = userName;
            return Task.CompletedTask;
        }

        public Task<string> GetNormalizedUserNameAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.NormalizedUserName);
        }

        public Task SetNormalizedUserNameAsync(AppIdentityUser user, string normalizedName, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (normalizedName == null)
            {
                throw new ArgumentNullException(nameof(normalizedName));
            }
            user.NormalizedUserName = normalizedName;
            return Task.CompletedTask;
        }

        public async Task<IdentityResult> CreateAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            try
            {
                await _userDataController.Create(user);
                return IdentityResult.Success;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return IdentityResult.Failed(new IdentityError() { Description = e.Message });
            }
        }

        public async Task<IdentityResult> UpdateAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            try
            {
                await _userDataController.Update(user);
                return IdentityResult.Success;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return IdentityResult.Failed(new IdentityError() { Description = e.Message });
            }
        }

        public async Task<IdentityResult> DeleteAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            try
            {
                await _userDataController.Delete(user);
                return IdentityResult.Success;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return IdentityResult.Failed(new IdentityError() { Description = e.Message });
            }
        }

        public async Task<AppIdentityUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId));
            }
            var user = await _userDataController.GetById(Guid.Parse(userId));
            if (user != null)
                user.UserName = user.Email;
            await AddRoles(user);
            return user;
        }

        public async Task<AppIdentityUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(normalizedUserName))
            {
                throw new ArgumentNullException(nameof(normalizedUserName));
            }

            var user = await _userDataController.GetByNormalizedName(normalizedUserName);
            if (user != null)
                user.UserName = user.Email;
            await AddRoles(user);
            return user;
        }

        public Task SetPasswordHashAsync(AppIdentityUser user, string passwordHash, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.PasswordHash = passwordHash;
            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(string.IsNullOrEmpty(user.PasswordHash));
        }

        public Task SetSecurityStampAsync(AppIdentityUser user, string stamp, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.SecurityStamp = stamp;
            return Task.CompletedTask;
        }

        public Task<string> GetSecurityStampAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.SecurityStamp);
        }

        public Task SetEmailAsync(AppIdentityUser user, string email, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.Email = email;
            return Task.CompletedTask;
        }

        public Task<string> GetEmailAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.EmailConfirmed);
        }

        public Task SetEmailConfirmedAsync(AppIdentityUser user, bool confirmed, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.EmailConfirmed = confirmed;
            return Task.CompletedTask;
        }

        public async Task<AppIdentityUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(normalizedEmail))
            {
                throw new ArgumentNullException(nameof(normalizedEmail));
            }
            var user = await _userDataController.GetByNormalizedEmail(normalizedEmail);
            if (user != null)
                user.UserName = user.Email;
            await AddRoles(user);

            return user;
        }

        public Task<string> GetNormalizedEmailAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.NormalizedEmail);
        }

        public Task SetNormalizedEmailAsync(AppIdentityUser user, string normalizedEmail, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (normalizedEmail == null)
            {
                throw new ArgumentNullException(nameof(normalizedEmail));
            }
            user.NormalizedEmail = normalizedEmail;
            return Task.CompletedTask;
        }

        public Task<DateTimeOffset?> GetLockoutEndDateAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.LockoutEnd);
        }

        public Task SetLockoutEndDateAsync(AppIdentityUser user, DateTimeOffset? lockoutEnd, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.LockoutEnd = lockoutEnd;
            return Task.CompletedTask;
        }

        public Task<int> IncrementAccessFailedCountAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.AccessFailedCount++;
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task ResetAccessFailedCountAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.AccessFailedCount = 0;
            return Task.CompletedTask;
        }

        public Task<int> GetAccessFailedCountAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<bool> GetLockoutEnabledAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.LockoutEnabled);
        }

        public Task SetLockoutEnabledAsync(AppIdentityUser user, bool enabled, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.LockoutEnabled = enabled;
            return Task.CompletedTask;
        }

        public Task AddToRoleAsync(AppIdentityUser user, string roleName, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            roleName = roleName.ToUpper();
            return _roleDataController.AddUserToRole(user, roleName);
        }

        public async Task RemoveFromRoleAsync(AppIdentityUser user, string roleName, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            throw new VerificationException("It is forbidden to delete the user role");
        }

        public Task<IList<string>> GetRolesAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<IList<string>>(new List<string>(){user.RoleName});
        }

        public Task<bool> IsInRoleAsync(AppIdentityUser user, string roleName, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(string.Equals(user.RoleName, roleName, StringComparison.OrdinalIgnoreCase));
        }

        public Task<IList<AppIdentityUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException(nameof(roleName));
            }
            return Task.FromResult<IList<AppIdentityUser>>(new List<AppIdentityUser>());
        }

        public Task<IList<Claim>> GetClaimsAsync(AppIdentityUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult<IList<Claim>>(user.Claims.ToList());
        }

        public Task AddClaimsAsync(AppIdentityUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            foreach (var claim in claims)
            {
                user.Claims.Add(claim);
            }
            return Task.CompletedTask;
        }

        public Task ReplaceClaimAsync(AppIdentityUser user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            user.Claims.Remove(claim);
            user.Claims.Add(newClaim);
            return Task.CompletedTask;
        }

        public Task RemoveClaimsAsync(AppIdentityUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            foreach (var claim in claims)
            {
                user.Claims.Remove(claim);
            }
            return Task.CompletedTask;
        }

        public Task<IList<AppIdentityUser>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            return Task.FromResult<IList<AppIdentityUser>>(new List<AppIdentityUser>());
        }
        
        private async Task AddRoles(AppIdentityUser user)
        {
            if (user != null)
            {
                var roles = await _roleDataController.GetByUserId(user.Id);
                user.RoleName = roles.FirstOrDefault()?.Name;
            }
        }
    }
}