
using Bid.Domain.Models;
using Bid.Domain.ViewModels;

namespace Bid.Business.Identity
{
    public interface IJwtTokenManager
    {
        TokenViewModel GetAuthToken(AppIdentityUser user);
        
    }
}