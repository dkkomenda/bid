using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Bid.Domain.ViewModels;
using Bid.Domain.Models;
using Bid.Domain.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Bid.Business.Identity
{
    public class JwtTokenManager : IJwtTokenManager
    {
        private IConfiguration _configuration;

        public JwtTokenManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public TokenViewModel GetAuthToken(AppIdentityUser user)
        {
            var refresh = Guid.NewGuid().ToString();

            var lifeTime = _configuration.Get<int>("JWTokenOptions:TokenDefaultLifeTimeInMinutes");
            var expires = DateTime.UtcNow.AddMinutes(lifeTime);
            var claims = new List<Claim>()
            {
                new (ClaimTypes.Email, user.Email),
                new (JwtRegisteredClaimNames.Jti, refresh),
                new (ClaimTypes.NameIdentifier, user.Id.ToString()),
                new (ClaimTypes.Expiration, expires.ToString())
            };

            if (user.Claims.Count > 0)
                claims.AddRange(user.Claims);
            
            claims.Add(new Claim(ClaimTypes.Role, user.RoleName.ToUpper()));
            var jwtSecurityToken = CreateToken(claims, expires);

            return new TokenViewModel()
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
                TokenType = "bearer",
                ExpiresIn = (int)jwtSecurityToken.ValidTo.Subtract(jwtSecurityToken.ValidFrom).TotalSeconds,
                RefreshToken = refresh,
                Login= user.Email,
                Id = user.Id,
                Role = user.RoleName.ToUpper(),
                Issued = jwtSecurityToken.ValidFrom,
                Expires = jwtSecurityToken.ValidTo
            };
        }

        private JwtSecurityToken CreateToken(IEnumerable<Claim> claims, int lifeTimeInMinutes)
        {
            return CreateToken(claims, DateTime.UtcNow.AddMinutes(lifeTimeInMinutes));
        }

        private JwtSecurityToken CreateToken(IEnumerable<Claim> claims, DateTime expires)
        {
            return new JwtSecurityToken
            (
                _configuration["JWTokenOptions:Issuer"],
                _configuration["JWTokenOptions:Audience"],
                claims,
                expires: expires,
                notBefore: DateTime.UtcNow,
                signingCredentials: CreateCredentials(_configuration["JWTokenOptions:SymmetricKey"])
            );
        }

        private static SigningCredentials CreateCredentials(string key)
        {
            return new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
                SecurityAlgorithms.HmacSha256);
        }
    }
}