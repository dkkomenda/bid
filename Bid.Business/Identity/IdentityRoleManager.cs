using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bid.DataAccess.DataControllers;
using Bid.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Bid.Business.Identity
{
    public class IdentityRoleManager : RoleManager<AppIdentityRole>
    {
        private readonly RolesDataController _roleDataController;

        public IdentityRoleManager(
            IEnumerable<IRoleValidator<AppIdentityRole>> roleValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            ILogger<RoleManager<AppIdentityRole>> logger,
            ILogger<IdentityRoleStore> identityRoleStoreLogger,
            RolesDataController roleDataController)
            : base(
                new IdentityRoleStore(roleDataController, identityRoleStoreLogger), 
                roleValidators,
                keyNormalizer, 
                errors, 
                logger)
        {
            _roleDataController = roleDataController;
        }

        public Task<IEnumerable<AppIdentityRole>> GetAllAsync()
        {
            return _roleDataController.GetAll();
        }

        public Task<AppIdentityRole> GetByNormalizedNameAsync(string normalizedName)
        {
            return Store.FindByNameAsync(normalizedName, System.Threading.CancellationToken.None);
        }

        public Task<IEnumerable<AppIdentityRole>> GetAllRoles()
        {
            return _roleDataController.GetAll();
        }

        public Task<AppIdentityRole> GetById(Guid id)
        {
            return _roleDataController.GetById(id);
        }


        /// <summary>
        /// Deletes the role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Task<bool> DeleteRole(Guid id)
        {
            return _roleDataController.Delete(id);
        }
    }
}