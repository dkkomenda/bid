
using Bid.DataAccess.DataControllers;
using Bid.DataAccess.DataControllers;
using Bid.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Bid.Business.Identity
{
     public class IdentityRoleStore : IRoleStore<AppIdentityRole>
    {
        private readonly RolesDataController _roleDataController;
        private readonly ILogger<IdentityRoleStore> _logger;

        public IdentityRoleStore(
            RolesDataController roleDataController,
            ILogger<IdentityRoleStore> logger)
        {
            _roleDataController = roleDataController;
            _logger = logger;
        }

        public void Dispose()
        {

        }

        public async Task<IdentityResult> CreateAsync(AppIdentityRole role, CancellationToken cancellationToken)
        {
            try
            {
                role.CreatedAt = role.UpdatedAt = DateTime.UtcNow;
                await _roleDataController.Update(role);
                return IdentityResult.Success;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return IdentityResult.Failed(new IdentityError() { Description = e.Message });
            }
        }

        public async Task<IdentityResult> UpdateAsync(AppIdentityRole role, CancellationToken cancellationToken)
        {
            try
            {
                await _roleDataController.Update(role);
                return IdentityResult.Success;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return IdentityResult.Failed(new IdentityError() { Description = e.Message });
            }
        }

        public Task<IdentityResult> DeleteAsync(AppIdentityRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(IdentityResult.Failed(new IdentityError() { Description = "Delete not implemented" }));
        }

        public Task<string> GetRoleIdAsync(AppIdentityRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.Id.ToString());
        }

        public Task<string> GetRoleNameAsync(AppIdentityRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.Name);
        }
        public Task SetRoleNameAsync(AppIdentityRole role, string roleName, CancellationToken cancellationToken)
        {
            role.Name = roleName;
            return Task.CompletedTask;
        }
        public Task<string> GetNormalizedRoleNameAsync(AppIdentityRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.NormalizedName);
        }
        public Task SetNormalizedRoleNameAsync(AppIdentityRole role, string normalizedName, CancellationToken cancellationToken)
        {
            role.NormalizedName = normalizedName;
            return Task.CompletedTask;
        }

        public Task<AppIdentityRole> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            return _roleDataController.GetById(Guid.Parse(roleId));
        }

        public Task<AppIdentityRole> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            return _roleDataController.GetByNormalizedName(normalizedRoleName);
        }
    }
}