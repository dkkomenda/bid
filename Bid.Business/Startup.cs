﻿using Bid.Business.Hubs;
using Bid.Business.Identity;
using Bid.Business.Interfaces;
using Bid.Business.Services;
using Bid.DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Bid.Business
{
    public static class Startup {
        public static void AddBusiness(this IServiceCollection services)
        {
            services.AddDataAccess();
            services.AddSingleton<IJwtTokenManager, JwtTokenManager>();
            services.AddSingleton<IEmailMessageSenderService, EmailMessageSenderService>();
            services.AddSingleton<IHubsService,HubsService>();
            services.AddSingleton<IUserSettingsService, UserSettingsService>();
            services.AddSingleton<IItemsService, ItemsService>();
            services.AddSingleton<IBidsService,BidsService>();
            services.AddSingleton<IAuctionsService, AuctionService>();
            services.AddSingleton<IImageService, ImageService>();
            
        }
        public static void UseBusiness(this IApplicationBuilder app)
        {
            app.UseDataAccess();
        }
    }
}

