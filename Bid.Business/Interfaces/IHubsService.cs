﻿using Bid.Domain.Models;

namespace Bid.Business.Interfaces;

public interface IHubsService
{
    Task OnSendNewItem(ItemBase model);
    Task OnSendUpdatedItem(ItemBase model, Guid userId);
    Task OnSendDeletedItem(Guid id);
    Task OnSendAuctionEnded(string itemName, string ItemPrice);
}