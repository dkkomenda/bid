﻿using Bid.Domain.Enums;
using Bid.Domain.Models;

namespace Bid.Business.Interfaces;

public interface IEmailMessageSenderService
{
    Task<ResponseMessageProviderEnum> SendMessage(string messageText, string messageTitle, string[] targets);
    Task<bool> PrepareMessage(WinnerLetterModel letterModel);

}