﻿using Bid.Domain.Enums;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;

namespace Bid.Business.Interfaces;

public interface IBidsService
{
    Task<bool> InsertOrUpdate(BidModel model);
    Task<IEnumerable<AuctionBidDetailModel>> GetByAuctionId(Guid auctionId);
    Task<int> GetLastBidByAuctionId(Guid auctionId);
    Task<IEnumerable<BidViewModel>> GetBidViewModelsByUserId(Guid userId);
    Task<bool> SetWonBidByAuctionId(Guid auctionId);
}