﻿using Bid.Domain.Models;

namespace Bid.Business.Interfaces;

public interface IItemsService
{
    Task<bool> Create(ItemBase item);
    Task<bool> Update(ItemBase item);
    Task<bool> Delete(Guid itemId);
    Task<bool> UpdateItemPriceById(Guid itemId, int price, Guid userId);
    Task<IEnumerable<ItemBase>> GetAllItems();
    Task<ItemBase> GetItemById(Guid itemId);
    Task<byte[]> GetImageByItemId(Guid itemId);
}