﻿using Bid.Domain.Models;

namespace Bid.Business.Interfaces;

public interface IAuctionsService
{
    Task<bool> InsertOrUpdate(AuctionModel model);
    Task<AuctionModel> GetById(Guid modelId);
    Task<bool> DeleteByItemId(Guid itemId);
    Task<bool> ChangeAuctionStatus(AuctionModel auction);
    Task<AuctionModel> GetByItemId(Guid itemId);
    Task<IEnumerable<AuctionModel>> GetActiveAuctions();
}