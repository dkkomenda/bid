﻿using System;
using System.Threading.Tasks;

namespace Bid.Business.Interfaces
{
    public interface IImageService
    {
        Task SaveImage(Guid id, string sectionName, byte[] data ,int width, int height);
        
        Task<byte[]> GetImage(Guid id, string sectionName);
        
        string CreateSection(string name);
        
        Task<byte[]> GetUserImage(Guid userId, bool isSmall);
        
        Task SaveUserImage(Guid userId, byte[] data);

        Task<string> GetImagePath(Guid id, string sectionName);
    }
}