﻿using Bid.Domain.Models;

namespace Bid.Business.Interfaces;

public interface IUserSettingsService
{
    Task<bool> InsertOrUpdate(UserSettingsModel model);
    
    //When user made new bid
    Task<bool> UpdateAvailableBalanceByUserIdAndAuctionId(Guid userId, Guid auctionId);
    
    //When item was deleted
    Task<bool> UpdateAvailableBidCountByItemId(Guid itemId);
    Task<UserSettingsModel> GetSettingsByUserId(Guid userId);
}