﻿using Microsoft.AspNetCore.SignalR;

namespace Bid.Business.Hubs;

public class CommunicationUiHub: Hub
{
    
    public CommunicationUiHub()
    {
    }

    public async override Task OnConnectedAsync()
    {
        try
        {
            Console.WriteLine("WebApi SignalR was connected. General hub");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
}