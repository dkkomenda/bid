﻿using Bid.Domain.Models;
using Microsoft.AspNetCore.SignalR;
namespace Bid.Business.Hubs;

public class BidCommunicationUiHub : Hub
{
    public BidCommunicationUiHub()
    { }
        
    public override async Task OnConnectedAsync()
    {
        var userData = Context.GetHttpContext().Request.Headers.TryGetValue("user_id", out var userId);
        if(!userData)
        {
            Console.WriteLine($"BidCommunicationUiHub: Connection to user hub aborted, user_id = {userId}");
            Context.Abort();
            return;
        }
            
        await Groups.AddToGroupAsync(Context.ConnectionId, userId.ToString().ToUpper());
        AuctionConnectedUsers.Ids.Add(userId.ToString(), Context.ConnectionId);
        Console.WriteLine($"BidCommunicationUiHub: Was connected user UI Hub, Id -> {userId}");
    }
    public override async Task OnDisconnectedAsync(Exception e)
    {
        var userData = Context.GetHttpContext().Request.Headers.TryGetValue("user_id", out var userId);
                
        if(!userData)
            return;
            
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, userId.ToString().ToUpper());
        AuctionConnectedUsers.Ids.Remove(userId.ToString());
        Console.WriteLine($"BidCommunicationUiHub: Was disconnected bid UI Hub, Id -> {userId}");
    }
}