(function (scope) {
    "use strict"
    scope.Timer = scope.Timer || {}
    scope.Timer.Init = _init;
    scope.Timer.RestartTimer = _restartTimer;
    scope.Timer.OnTimesUp = _onTimesUp;

    const FULL_DASH_ARRAY = 283;
    const WARNING_THRESHOLD = 10;
    const ALERT_THRESHOLD = 5;

    const COLOR_CODES = {
        info: {
            color: "green"
        },
        warning: {
            color: "orange",
            threshold: WARNING_THRESHOLD
        },
        alert: {
            color: "red",
            threshold: ALERT_THRESHOLD
        }
    };

    let timeLeft = 0;
    let _ref = {};
    let TIME_LIMIT = 0;
    let timePassed = 0;
    let timerInterval = null;
    let remainingPathColor = COLOR_CODES.info.color;
    
    function _init(reference,timeTotal,timeRest,Id){
        _ref = reference;
        TIME_LIMIT = timeTotal;
        timePassed = timeTotal-timeRest;
        
        timeLeft = TIME_LIMIT - timePassed;
        _setLayout(Id);
        startTimer();
    }
    
    function _onTimesUp(needToInvoke = true) {
        clearInterval(timerInterval);
        
        if(needToInvoke)
            _ref.invokeMethodAsync("TimerFinish");
    }

    function _restartTimer(timeTotal,timeRest){
        clearInterval(timerInterval);
        timePassed = 0;
        TIME_LIMIT = timeTotal;
        timePassed = timeTotal-timeRest;
        timeLeft = TIME_LIMIT - timePassed;
        startTimer();
    }

    function startTimer() {
        timerInterval = setInterval(() => {
            timePassed = timePassed += 1;
            timeLeft = TIME_LIMIT - timePassed;
            document.getElementById("base-timer-label").innerHTML = formatTime(
                timeLeft
            );
            setCircleDasharray();
            setRemainingPathColor(timeLeft);

            if (timeLeft <= 0) {
                _onTimesUp(true);
            }
        }, 1000);
    }
    
    function formatTime(time) {
        var sec_num = parseInt(time, 10); 
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        
        return`${hours}:${minutes}:${seconds}`
    }

    function setRemainingPathColor(timeLeft) {
        const { alert, warning, info } = COLOR_CODES;
        if (timeLeft <= alert.threshold) {
            document
                .getElementById("base-timer-path-remaining")
                .classList.remove(warning.color);
            document
                .getElementById("base-timer-path-remaining")
                .classList.add(alert.color);
        } else if (timeLeft <= warning.threshold) {
            document
                .getElementById("base-timer-path-remaining")
                .classList.remove(info.color);
            document
                .getElementById("base-timer-path-remaining")
                .classList.add(warning.color);
        }
    }

    function calculateTimeFraction() {
        const rawTimeFraction = timeLeft / TIME_LIMIT;
        return rawTimeFraction - (1 / TIME_LIMIT) * (1 - rawTimeFraction);
    }

    function setCircleDasharray() {
        const circleDasharray = `${(
            calculateTimeFraction() * FULL_DASH_ARRAY
        ).toFixed(0)} 283`;
        document
            .getElementById("base-timer-path-remaining")
            .setAttribute("stroke-dasharray", circleDasharray);
    }
    
    function _setLayout(Id){
        document.getElementById("app").innerHTML = `
        <div class="base-timer" style="transform: rotateY(180deg);">
          <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
            <g class="base-timer__circle">
              <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45"></circle>
              <path
                id="base-timer-path-remaining"
                stroke-dasharray="283"
                class="base-timer__path-remaining ${remainingPathColor}"
                d="
                  M 50, 50
                  m -45, 0
                  a 45,45 0 1,0 90,0
                  a 45,45 0 1,0 -90,0
                "
              ></path>
            </g>
          </svg>
          <img src="/image/GetItemImageById/${Id}" alt="Image" class="base-timer__image"/>
          <span id="base-timer-label" class="base-timer__label">${formatTime(
                    timeLeft
                )}</span>
        </div>
        `;
    }
})(window)

















