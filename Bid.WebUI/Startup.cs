using Bid.WebUI.Hubs;
using Bid.WebUI.Services;
using Bid.WebUI.Services.HttpServices;
using Blazored.LocalStorage;
using Blazored.Modal;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Bid.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddBlazoredLocalStorage();
            services.AddAuthorizationCore();
            services.AddRazorPages();
            services.AddBlazoredModal();
            services.AddControllers();
            services.AddServerSideBlazor(o => o.DetailedErrors = true);
            services.AddHttpContextAccessor();

            services.AddScoped<AuthenticationStateProvider, CustomAuthStateProvider>();
            services.AddTransient<AuthorizationService>();
            services.AddTransient<ItemsHttpService>();
            services.AddTransient<BidsHttpService>();
            services.AddTransient<UserSettingsHttpService>();
            services.AddTransient<AuctionsHttpService>();
            
            services.AddScoped<ToastService>();
            services.AddScoped<HubServiceUi>();
            services.AddScoped<BidHubServiceUi>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
