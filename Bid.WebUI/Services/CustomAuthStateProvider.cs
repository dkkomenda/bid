using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Bid.WebUI.Services.HttpServices;
using Bid.Domain.ViewModels;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Newtonsoft.Json;
using Bid.Domain.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Bid.WebUI.Services
{
    public class CustomAuthStateProvider : AuthenticationStateProvider
    {
        private readonly ILocalStorageService _localStorage;
        private readonly AuthorizationService _authorizationService;
        private readonly IConfiguration _configuration;
        private readonly ILogger<CustomAuthStateProvider> _logger;
        protected NavigationManager NavigationManager { get; set; }

        public CustomAuthStateProvider(
            ILocalStorageService localStorage, 
            AuthorizationService authorizationService,
            NavigationManager navigationManager, 
            IConfiguration configuration,
            ILogger<CustomAuthStateProvider> logger)
        {
            _localStorage = localStorage;
            _authorizationService = authorizationService;
            NavigationManager = navigationManager;
            _configuration = configuration;
            _logger = logger;
        }
        
        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            try
            {
                string userToken = await GetTokenAsync();

                if (string.IsNullOrWhiteSpace(userToken))
                {
                    return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
                }

                return new AuthenticationState(
                    new ClaimsPrincipal(new ClaimsIdentity(ParseClaimsFromJwt(userToken), "jwt")));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
            }
        }
        public async Task<string> GetTokenAsync()
        {
            try
            {
                var jwt = await _localStorage.GetItemAsync<object>("_jwt");

                if (jwt == null)
                {
                    MarkUserAsLoggedOut();
                    return null;
                }

                var deserializedToken = JsonConvert.DeserializeObject<TokenViewModel>(jwt.ToString());

                var jwthandler = new JwtSecurityTokenHandler();
                var jwttoken = jwthandler.ReadToken(deserializedToken.AccessToken);
                var expDate = jwttoken.ValidTo;

                if (expDate <= DateTime.UtcNow)
                {
                    MarkUserAsLoggedOut();
                    return null;
                }
                
                if (expDate < DateTime.UtcNow.AddMinutes(_configuration.Get<int>("RefreshTokenTimeMinute")))
                {
                    await RefreshToken(deserializedToken);
                }
                
                return deserializedToken.AccessToken;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                LogOut();
                return null;
            }
        }
        
        public void MarkUserAsAuthenticated(string email)
        {
            var authenticatedUser =
                new ClaimsPrincipal(new ClaimsIdentity(new[] {new Claim(ClaimTypes.Name, email)}, "apiauth"));
            var authState = Task.FromResult(new AuthenticationState(authenticatedUser));
            NotifyAuthenticationStateChanged(authState);
        }

        private void MarkUserAsLoggedOut()
        {
            var anonymousUser = new ClaimsPrincipal(new ClaimsIdentity());
            var authState = Task.FromResult(new AuthenticationState(anonymousUser));
            NotifyAuthenticationStateChanged(authState);
        }

        public void LogOut()
        {
            MarkUserAsLoggedOut();
            _localStorage.RemoveItemAsync("_jwt");
            NavigationManager.NavigateTo("/login", true);
        }
        public IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var claims = new List<Claim>();
            var payload = jwt.Split('.')[1];
            var jsonBytes = ParseBase64WithoutPadding(payload);
            var keyValuePairs = System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);

            keyValuePairs.TryGetValue(ClaimTypes.Role, out object roles);

            if (roles != null)
            {
                if (roles.ToString().Trim().StartsWith("["))
                {
                    var parsedRoles = System.Text.Json.JsonSerializer.Deserialize<string[]>(roles.ToString());

                    foreach (var parsedRole in parsedRoles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, parsedRole));
                    }
                }
                else
                {
                    claims.Add(new Claim(ClaimTypes.Role, roles.ToString()));
                }

                keyValuePairs.Remove(ClaimTypes.Role);
            }

            claims.AddRange(keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString())));

            return claims;
        }
        private byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2:
                    base64 += "==";
                    break;
                case 3:
                    base64 += "=";
                    break;
            }

            return Convert.FromBase64String(base64);
        }
        private async Task<bool> RefreshToken(TokenViewModel deserializedToken)
        {
            var newTokenViewModel =
                await _authorizationService.RefreshAuthToken(deserializedToken.RefreshToken);
                    
            if (newTokenViewModel == null)
            {
                LogOut();
                return false;
            }
                    
            deserializedToken = newTokenViewModel;
            await _localStorage.SetItemAsync("_jwt", JsonConvert.SerializeObject(deserializedToken));
            
            return true;
        }
        public async Task<bool> RefreshToken()
        {
            var jwt = await _localStorage.GetItemAsync<object>("_jwt");
            var deserializedToken = JsonConvert.DeserializeObject<TokenViewModel>(jwt.ToString());

            return await RefreshToken(deserializedToken);
        }

        public async Task CheckIsNeedRefresh(DateTime endTime)
        {
            if (endTime < DateTime.UtcNow.AddMinutes(_configuration.Get<int>("RefreshTokenTimeMinute")))
                await RefreshToken();
        }
    }
}