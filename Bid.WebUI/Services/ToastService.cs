﻿using System;
using System.Threading.Tasks;
using System.Timers;
using Bid.Domain.Enums;

namespace Bid.WebUI.Services;

public class ToastService
{
    public event Action<string, ToastLevel> OnShow;
    public event Action OnHide;
    
    private Timer Countdown;
    public async Task ShowToast(string message, ToastLevel level)
    {
        OnShow?.Invoke(message, level);
        StartCountdown();
    }
    private void StartCountdown()
    {
        SetCountdown();

        if (Countdown.Enabled)
        {
            Countdown.Stop();
            Countdown.Start();
        }
        else
        {
            Countdown.Start();
        }
    }
    private void SetCountdown()
    {
        if (Countdown == null)
        {
            Countdown = new Timer(2500);
            Countdown.Elapsed += HideToast;
            Countdown.AutoReset = false;
        }
    }
    private void HideToast(object source, ElapsedEventArgs args)
    {
        OnHide?.Invoke();
    }
}