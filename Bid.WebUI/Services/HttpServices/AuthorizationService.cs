using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Blazored.LocalStorage;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;


namespace Bid.WebUI.Services.HttpServices
{
    public class AuthorizationService : HttpServiceBase
    {
        protected override string _apiControllerName { get; set; }
        private TokenViewModel Token { get; set; }
        public AuthorizationService(IConfiguration configuration, ILocalStorageService localStorageService)
            : base(configuration, localStorageService)
        {
            _apiControllerName = "Auth";
        }
        public async Task<TokenViewModel> Login(LoginViewModel model)
        {
            var response = await _client.PostAsync(Url("Login"),
                new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));

            Token = await response.Content.ReadAsAsync<TokenViewModel>();
            return Token;
        }
        public async Task<TokenViewModel> RefreshAuthToken(string refreshToken)
        {
            await AddAuthorizationAsync(true);
            var response = await _client.GetAsync(Url($"RefreshAuthToken/{refreshToken}"));
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            Token = await response.Content.ReadAsAsync<TokenViewModel>();
            return Token;
        }
        public async Task<RequestResponseViewModel> ChangePassword(ChangePasswordViewModel model)
        {
            await AddAuthorizationAsync(true);
            var response = await _client.PostAsync(Url("ChangePassword"),
                new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            return await GetRequestResponseAsync(response);
        }
        public async Task<Guid> GetCurrentUserId()
        {
            await AddAuthorizationAsync(true);
            
            var response = await _client.GetAsync(Url($"GetCurrentUserId"));
            if (!response.IsSuccessStatusCode)
            {
                return Guid.Empty;
            }

            var userId = await response.Content.ReadAsAsync<Guid>();
            return userId;
        }
        
        public async Task<AppIdentityUser> GetCurrentUserById(Guid userId)
        {
            await AddAuthorizationAsync(true);
            
            var response = await _client.GetAsync(Url($"GetCurrentUserById/{userId}"));
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var user = await response.Content.ReadAsAsync<AppIdentityUser>();
            return user;
        }
    }
}