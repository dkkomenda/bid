﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Bid.WebUI.Services.HttpServices
{
    public class ItemsHttpService: HttpServiceBase
    {
        protected override string _apiControllerName { get; set; }

        public ItemsHttpService(IConfiguration configuration,
            ILocalStorageService localStorage, 
            AuthenticationStateProvider authenticationStateProvider)
            : base(configuration, localStorage, authenticationStateProvider)
        {
            _apiControllerName = "Items";
        }
        public async Task<RequestResponseViewModel> Create(ItemModel model)
        {
            await AddAuthorizationAsync();
            var response = await _client.PostAsync(Url("Create"),  new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            if (!response.IsSuccessStatusCode)
                return null;
            return await GetRequestResponseAsync(response);
        }
        public async Task<RequestResponseViewModel> Update(ItemModel model)
        {
            await AddAuthorizationAsync();
            var response = await _client.PostAsync(Url("Update"),  new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            if (!response.IsSuccessStatusCode)
                return null;
            return await GetRequestResponseAsync(response);
        }
        public async Task<IEnumerable<ItemBase>> GetItems()
        {
            await AddAuthorizationAsync();
            var response = await _client.GetAsync(Url("GetAllItems"));
            if (!response.IsSuccessStatusCode)
                return null;
            return await response.Content.ReadAsAsync<IEnumerable<ItemBase>>();
        }
        public async Task<RequestResponseViewModel> Delete(Guid id)
        {
            await AddAuthorizationAsync();
            var response = await _client.DeleteAsync(Url($"Delete/{id}"));
            if (!response.IsSuccessStatusCode)
                return null;
            return await GetRequestResponseAsync(response);
        }
        public async Task<ItemModel> GetItemById(Guid id)
        {
            await AddAuthorizationAsync();
            var response = await _client.GetAsync(Url($"GetItemById/{id}"));
            if (!response.IsSuccessStatusCode)
                return null;
            return await response.Content.ReadAsAsync<ItemModel>();
        }
        public async Task<byte[]> GetItemImageById(Guid itemId)
        {
            await AddAuthorizationAsync();
            var response = await _client.GetAsync(Url($"GetImageById/{itemId}"));
            if (!response.IsSuccessStatusCode)
                return null;
            return await response.Content.ReadAsByteArrayAsync();
        }
    }
}