﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Bid.WebUI.Services.HttpServices;

public class BidsHttpService: HttpServiceBase
{
    protected override string _apiControllerName { get; set; }

    public BidsHttpService(IConfiguration configuration,
        ILocalStorageService localStorage, 
        AuthenticationStateProvider authenticationStateProvider)
        : base(configuration, localStorage, authenticationStateProvider)
    {
        _apiControllerName = "Bids";
    }

    public async Task<IEnumerable<AuctionBidDetailModel>> GetBidsByAuctionId(Guid auctionId)
    {
        await AddAuthorizationAsync();
        var response = await _client.GetAsync(Url($"GetBidsByAuctionId/{auctionId}"));
        if (!response.IsSuccessStatusCode)
            return null;
        return await response.Content.ReadAsAsync<IEnumerable<AuctionBidDetailModel>>();
    }
    
    public async Task<RequestResponseViewModel> CreateBid(BidModel model)
    {
        await AddAuthorizationAsync();
        var response = await _client.PostAsync(Url($"CreateBid"),  new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
        if (!response.IsSuccessStatusCode)
            return null;
        return await GetRequestResponseAsync(response);
    }
    
    public async Task<IEnumerable<BidViewModel>> GetBidViewModelsByUserId(Guid userId)
    {
        await AddAuthorizationAsync();
        var response = await _client.GetAsync(Url($"GetBidViewModelsByUserId/{userId}"));
        if (!response.IsSuccessStatusCode)
            return null;
        return await response.Content.ReadAsAsync<IEnumerable<BidViewModel>>();
    }
}