﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Bid.WebUI.Services.HttpServices;

public class UserSettingsHttpService : HttpServiceBase
{
    protected override string _apiControllerName { get; set; }

    public UserSettingsHttpService(IConfiguration configuration, ILocalStorageService localStorage) : base(configuration, localStorage)
    {
        _apiControllerName = "UserSettings";
    }
    public async Task<UserSettingsModel> GetUserSettingsById(Guid userId)
    {
        await AddAuthorizationAsync();
        var response = await _client.GetAsync(Url($"GetUserSettingsById/{userId}"));
        if (!response.IsSuccessStatusCode)
            return null;
        return await response.Content.ReadAsAsync<UserSettingsModel>();
    }
    public async Task<RequestResponseViewModel> UpdateUserSettings(UserSettingsModel model)
    {
        await AddAuthorizationAsync();
        var response = await _client.PostAsync(Url("Update"),  new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
        if (!response.IsSuccessStatusCode)
            return null;
        return await GetRequestResponseAsync(response);
    }
    // public async Task<RequestResponseViewModel> UpdateAvailableBalanceByUserIdAndAuctionId(Guid userId, Guid auctionId)
    // {
    //     await AddAuthorizationAsync();
    //     var response = await _client.GetAsync(Url($"UpdateAvailableBalanceByUserIdAndAuctionId/{userId}/{auctionId}"));
    //     if (!response.IsSuccessStatusCode)
    //         return null;
    //     return await GetRequestResponseAsync(response);
    // }
    
}