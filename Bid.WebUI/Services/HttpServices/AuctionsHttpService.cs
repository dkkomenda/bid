﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Bid.WebUI.Services.HttpServices;

public class AuctionsHttpService: HttpServiceBase
{
    protected override string _apiControllerName { get; set; }

    public AuctionsHttpService(IConfiguration configuration,
        ILocalStorageService localStorage,
        AuthenticationStateProvider authenticationStateProvider)
        : base(configuration, localStorage, authenticationStateProvider)
    {
        _apiControllerName = "Auctions";
    }
    public async Task<RequestResponseViewModel> Create(AuctionModel model)
    {
        await AddAuthorizationAsync();
        var response = await _client.PostAsync(Url($"CreateAuction"),  new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
        if (!response.IsSuccessStatusCode)
            return null;
        return await GetRequestResponseAsync(response);
    }
    
    public async Task<RequestResponseViewModel> DeleteByItemId(Guid itemId)
    {
        await AddAuthorizationAsync();
        var response = await _client.DeleteAsync(Url($"DeleteByItemId/{itemId}"));
        if (!response.IsSuccessStatusCode)
            return null;
        return await GetRequestResponseAsync(response);
    }
    public async Task<AuctionModel> GetByItemId(Guid itemId)
    {
        await AddAuthorizationAsync();
        var response = await _client.GetAsync(Url($"GetByItemId/{itemId}"));
        if (!response.IsSuccessStatusCode)
            return null;
        return await response.Content.ReadAsAsync<AuctionModel>();
    }
}