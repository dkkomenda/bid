﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Bid.Domain.ViewModels;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Bid.WebUI.Services.HttpServices
{
   public abstract class HttpServiceBase
    {
        protected readonly HttpClient _client;
        private IConfiguration _configuration { get; set; }
        private ILocalStorageService LocalStorage { get; set; }
        private AuthenticationStateProvider _authenticationStateProvider { get; set; }
        protected abstract string _apiControllerName { get; set; }
        protected HttpServiceBase(IConfiguration configuration, ILocalStorageService localStorage)
        {
            _configuration = configuration;
            LocalStorage = localStorage;


            if (string.IsNullOrEmpty(_configuration["WebApiUrl"]))
                return;

            _client = new HttpClient();
            _client.BaseAddress = new Uri(_configuration["WebApiUrl"]);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.Timeout = TimeSpan.FromMinutes(5);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        protected HttpServiceBase(IConfiguration configuration, ILocalStorageService localStorage,
            AuthenticationStateProvider authenticationStateProvider)
        {
            _configuration = configuration;
            LocalStorage = localStorage;
            _authenticationStateProvider = authenticationStateProvider;


            if (string.IsNullOrEmpty(_configuration["WebApiUrl"]))
                return;

            _client = new HttpClient();
            _client.BaseAddress = new Uri(_configuration["WebApiUrl"]);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.Timeout = TimeSpan.FromMinutes(5);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        protected async Task AddAuthorizationAsync(bool skipAuthStateProviderVerification = false)
        {
            string token = string.Empty;
        
            if (skipAuthStateProviderVerification)
                token = await GetTokenFromLocalstorage();
            else
            {
                try
                {
                    token = await ((CustomAuthStateProvider) _authenticationStateProvider).GetTokenAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        
            if (string.IsNullOrEmpty(token))
                return;
        
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }
        private async Task<string> GetTokenFromLocalstorage()
        {
            var jwt = await LocalStorage.GetItemAsync<object>("_jwt");

            if (jwt == null)
                return string.Empty;

            var deserializedToken = JsonConvert.DeserializeObject<TokenViewModel>(jwt.ToString());

            if (deserializedToken == null)
                return string.Empty;

            return deserializedToken.AccessToken;
        }
        protected void ClearAuthorization()
        {
            _client.DefaultRequestHeaders.Authorization = null;
        }
        protected string Url()
        {
            return $"/api/{_apiControllerName}";
        }
        protected string Url(string action)
        {
            return $"/api/{_apiControllerName}/{action}";
        }
        protected async Task<RequestResponseViewModel> GetRequestResponseAsync(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return new RequestResponseViewModel {Succeeded = true};

            var content = await response.Content.ReadAsStringAsync();
            var result = string.IsNullOrEmpty(content)
                ? new RequestResponseViewModel()
                : JsonConvert.DeserializeObject<RequestResponseViewModel>(content);
            result.Succeeded = response.IsSuccessStatusCode;
            result.StatusCode = response.StatusCode.ToString();

            return result;
        }
    }
   
}