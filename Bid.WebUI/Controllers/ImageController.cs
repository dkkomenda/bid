﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using Bid.Domain.Helpers;
using Bid.WebUI.Services.HttpServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Bid.WebUI.Controllers
{
    [Route("[controller]/")]
    public class ImageController : Controller
    {
        private readonly ILogger<ImageController> _logger;
        private ItemsHttpService ItemsService { get; set; }
        private string DefaultImage { get; set; } = "wwwroot/img/100.png";

        public ImageController(ItemsHttpService itemsService, ILogger<ImageController> logger)
        {
            ItemsService = itemsService;
            _logger = logger;
        }
        
        [HttpGet("GetItemImageById/{itemId}")]
        public async Task<IActionResult> GetItemImageById(Guid itemId)
        {
            try
            {
                var itemImage = await ItemsService.GetItemImageById(itemId);

                var content = itemImage ?? Image.FromFile(DefaultImage)?.ImageToByteArray();
                return File(content, "image/png");
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null;
            }
        }
    }
}