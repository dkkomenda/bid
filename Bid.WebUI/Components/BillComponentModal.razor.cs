﻿using System;
using System.Threading.Tasks;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Bid.WebUI.Services.HttpServices;
using Blazored.Modal;
using Microsoft.AspNetCore.Components;

namespace Bid.WebUI.Components;

public partial class BillComponentModal
{
    [CascadingParameter] BlazoredModalInstance ModalInstance { get; set; }
    [Parameter] public Guid UserId { get; set; }
    [Parameter] public BidViewModel Bill { get; set; }
    [Inject] private AuthorizationService UserAuthorizationService { get; set; }
    private AppIdentityUser User { get; set; }

    protected override async Task OnInitializedAsync()
    {
        User = await UserAuthorizationService.GetCurrentUserById(UserId);
    }
}