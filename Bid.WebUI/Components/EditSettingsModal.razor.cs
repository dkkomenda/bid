﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Bid.Domain.Enums;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Bid.WebUI.Hubs;
using Bid.WebUI.Services;
using Bid.WebUI.Services.HttpServices;
using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;

namespace Bid.WebUI.Components;

public partial class EditSettingsModal
{
    [CascadingParameter] BlazoredModalInstance ModalInstance { get; set; }
    [Inject] public IModalService Modal { get; set; }
    [Parameter] public Guid UserId { get; set; }
    [Inject] private UserSettingsHttpService UserSettingsService { get; set; }
    [Inject] private BidsHttpService BidsService { get; set; }
    [Inject] private ToastService ToastService { get; set; }
    private UserSettingsModel UserSettings { get; set; } = new();
    private List<BidViewModel> Bids { get; set; } = new();
    protected bool OrderByDesc { get; set; } = false;
    protected string OrderPropertyName { get; set; } = nameof(BidViewModel.Date);

    protected override async Task OnInitializedAsync()
    {
        UserSettings = await UserSettingsService.GetUserSettingsById(UserId);
        Bids = (await BidsService.GetBidViewModelsByUserId(UserId)).OrderByDescending(r => r.GetType().GetProperty(OrderPropertyName).GetValue(r)).ToList();
    }
    protected async Task UpdateSettings(EditContext editContext)
    {
        RequestResponseViewModel responseSave = null;

        bool isValid = editContext.Validate();
        if (!isValid)
            return;

        responseSave = await UserSettingsService.UpdateUserSettings(UserSettings);

        if (!responseSave.Succeeded)
        {
            await ModalInstance.CloseAsync();
            await ToastService.ShowToast("Settings was not updated", ToastLevel.Error);
            return;
        }
        
        await ModalInstance.CloseAsync();
        await ToastService.ShowToast("Settings updated successfully", ToastLevel.Success);
        StateHasChanged();
    }
    
    protected void OrderClick(string propertyName)
    {
        if (propertyName == OrderPropertyName)
            OrderByDesc = !OrderByDesc;
        else
        {
            OrderPropertyName = propertyName;
            OrderByDesc = false;
        }

        GetOrderedPagedItems();
        StateHasChanged();
    }
    private void GetOrderedPagedItems()
    {
        if (OrderByDesc)
            Bids = Bids.OrderByDescending(r => r.GetType().GetProperty(OrderPropertyName).GetValue(r)).ToList();
        else
            Bids = Bids.OrderBy(r => r.GetType().GetProperty(OrderPropertyName).GetValue(r)).ToList();
    }
    protected void CancelModal()
    {
        ModalInstance.CancelAsync();
    }

    private void ShowBill(BidViewModel item)
    {
        var parameters = new ModalParameters();
        parameters.Add("Bill", item);
        parameters.Add("UserId", UserId);
        
        Modal.Show<BillComponentModal>("Bill", parameters);
    }
}