﻿using System;
using System.IO;
using System.Threading.Tasks;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Bid.WebUI.Services.HttpServices;
using Blazored.Modal;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace Bid.WebUI.Components
{
    public partial class EditItemModal
    {
        [CascadingParameter] BlazoredModalInstance ModalInstance { get; set; }
        [Parameter] public ItemModel Model { get; set; }
        [Inject] private ItemsHttpService ItemsService { get; set; }
        [Inject] private AuctionsHttpService AuctionsService { get; set; }
        private string ItemImage { get; set; } = "images/100.png";
        private AuctionModel Auction { get; set; }
        protected override async Task OnInitializedAsync()
        {
            Auction = new AuctionModel();
            if (Model.Id != Guid.Empty)
            {
                Auction = await AuctionsService.GetByItemId(Model.Id);
                var image = await ItemsService.GetItemImageById(Model.Id);
                if (image == null)
                {
                    ItemImage = "img/100.png";
                }
                else
                {
                    ItemImage = $"data:image/png;base64,{Convert.ToBase64String(image)}";
                    Model.Logo = image;
                }
            }
            else
            {
                Model.StartedAt = Model.EndedAt = DateTime.Now;
                Model.StartedAt = Model.StartedAt.AddSeconds(-Model.StartedAt.Second);
                Model.EndedAt = Model.StartedAt.AddSeconds(-Model.StartedAt.Second).AddHours(1);
                Model.Logo = File.ReadAllBytes(Environment.CurrentDirectory + "/wwwroot/" + ItemImage);
            }
        }
        protected async Task SaveItem(EditContext editContext)
        {
            bool responseSave;

            bool isValid = editContext.Validate();
            if (!isValid)
                return;

            Model.Image = "";
            if (Model.Id == Guid.Empty)
            {
                Model.Id = Guid.NewGuid();
                responseSave =  await Modify("Create");
            }
            else
            {
                responseSave =  await Modify("Update");
            }
            
            if (!responseSave)
                return;
            
            await ModalInstance.CloseAsync();
            StateHasChanged();
        }
        private async Task<bool> Modify(string keyWord)
        {
            RequestResponseViewModel res = null;
            RequestResponseViewModel res1 = null;
            Auction.ItemId = Model.Id;
            Auction.IsActive = Model.EndedAt >= DateTime.Now;
            Auction.StartedAt = Model.StartedAt;
            Auction.EndedAt = Model.EndedAt;
            switch (keyWord)
            {
                case "Create":
                    Auction.Id = Guid.NewGuid();
                    res = await ItemsService.Create(Model);
                    res1 = await AuctionsService.Create(Auction);
                break;
                case "Update":
                    res = await ItemsService.Update(Model);
                    res1 = await AuctionsService.Create(Auction);
                    break;
            }
            if (res == null || res1 == null)
                return false;
            return res.Succeeded && res1.Succeeded;
        }
        protected async Task HandleFileSelected(InputFileChangeEventArgs e)
        {
            var file = e.File;
            
            using (var ms = new MemoryStream())
            {
                using (var stream = file.OpenReadStream(5300000))
                {
                    await stream.CopyToAsync(ms);
                    Model.Logo = ms.ToArray();
                    ItemImage = $"data:image/png;base64,{Convert.ToBase64String(Model.Logo)}";
                }
            }
        }
        protected void CancelModal()
        {
            ModalInstance.CancelAsync();
        }
    }
}