﻿using Microsoft.AspNetCore.Components;
using System;
using Bid.Domain.ViewModels;

namespace Bid.WebUI.Components
{
    public partial class Pager
    {
        [Parameter]
        public PagedResultBase PagedInfo { get; set; }
        
        [Parameter]
        public Action<int> PageChanged { get; set; }
        
        [Parameter]
        public Action<int> ChangePageSize { get; set; }
        
        [Parameter] 
        public bool IsShowLeftBlock { get; set; } = false;
        protected int FirstIndexItem { get; set; }
        protected int LastIndexItem { get; set; }
        protected int LastSelectValue { get; set; } = 10;
        protected void PagerButtonClicked(int page)
        {
            PageChanged?.Invoke(page);
        }
        protected void OnChangePageSize(ChangeEventArgs e)
        {
            var selectValue = e.Value.ToString();
            LastSelectValue = Convert.ToInt32(selectValue);

            if (PagedInfo.TotalItems <= LastSelectValue)
            {
                PagedInfo.PageCount = 1;
            }
            else
            {
                PagedInfo.PageCount = (int)Math.Ceiling((decimal)PagedInfo.TotalItems / LastSelectValue);
            }

            if (PagedInfo.PageCount < PagedInfo.CurrentPage)
                PageChanged?.Invoke(PagedInfo.PageCount);
            ChangePageSize?.Invoke(LastSelectValue);
            StateHasChanged();
        }
    }
}
