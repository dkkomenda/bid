﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bid.Domain.Enums;
using Bid.Domain.Models;
using Bid.WebUI.Hubs;
using Bid.WebUI.Services;
using Bid.WebUI.Services.HttpServices;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace Bid.WebUI.Pages;

public partial class BidPage : IDisposable
{
    [Parameter] public string ItemId { get; set; }
    [Inject] private NavigationManager NavigationManager { get; set; }
    [Inject] private AuthorizationService AuthorizationService { get; set; }
    [Inject] private BidHubServiceUi BidHubServiceUi { get; set; }
    [Inject] private IJSRuntime JsRuntime { get; set; }
    [Inject] private ItemsHttpService ItemsService { get; set; }
    [Inject] private AuctionsHttpService AuctionsService { get; set; }
    [Inject] private UserSettingsHttpService UserSettingsService { get; set; }
    [Inject] private BidsHttpService BidsService { get; set; }
    [Inject] private ToastService ToastService { get; set; }
    private ItemBase ItemModel { get; set; } = new();
    private AuctionModel AuctionModel { get; set; }
    private UserSettingsModel UserSettings { get; set; } = new();
    private Dictionary<double,double> BidTime { get; set; }
    private bool IsAutoBiddingEnabled { get; set; }
    private Guid UserId { get; set; }
    private int LootStartPrice { get; set; }
    private List<AuctionBidDetailModel> BidsList { get; set; } = new();
    protected bool OrderByDesc { get; set; } = false;
    protected string OrderPropertyName { get; set; } = nameof(AuctionBidDetailModel.Date);
    protected override async Task OnInitializedAsync()
    {
        UserId = await AuthorizationService.GetCurrentUserId();
        AuctionModel = await AuctionsService.GetByItemId(Guid.Parse(ItemId));
        ItemModel = await ItemsService.GetItemById(Guid.Parse(ItemId));
        LootStartPrice = ItemModel.Price;
        BidsList =  (await BidsService.GetBidsByAuctionId(AuctionModel.Id))?.ToList() ?? new List<AuctionBidDetailModel>();
        UserSettings = await UserSettingsService.GetUserSettingsById(UserId);
        BidTime = GetBidTime();
        StateHasChanged();
    }
    private Dictionary<double, double> GetBidTime()
    {
        var total = ItemModel.EndedAt.Subtract(ItemModel.StartedAt).TotalSeconds;
        var rest = ItemModel.EndedAt.Subtract(DateTime.Now).TotalSeconds;
        Dictionary<double, double> res = new Dictionary<double, double>();
        res.Add(total,rest);
        return res;
    }
    protected override async Task OnAfterRenderAsync(bool isFirstRender)
    {
        await Task.Delay(300);
        if (isFirstRender)
        {
            if (UserId != Guid.Empty)
            {
                await BidHubServiceUi.ConnectAsync(UserId);
                BidHubServiceUi.NotifyItemChanged += OnItemSettingsChanged;
            }
            else
                return;
            
            await JsRuntime.InvokeVoidAsync("Timer.Init", DotNetObjectReference.Create(this), BidTime.Keys.FirstOrDefault(),BidTime.Values.FirstOrDefault(), ItemModel.Id);
        }
    }
    private async void OnItemSettingsChanged(ItemBase itemBase,Guid userId)
    {
        BidsList =  (await BidsService.GetBidsByAuctionId(AuctionModel.Id))?.ToList() ?? new List<AuctionBidDetailModel>();
        BidsList = BidsList.OrderByDescending(r => r.GetType().GetProperty(OrderPropertyName).GetValue(r)).ToList();
        UserSettings = await UserSettingsService.GetUserSettingsById(UserId);
        if(ItemModel.EndedAt != itemBase.EndedAt)
            RestartTimer(itemBase.EndedAt,itemBase.StartedAt);
        ItemModel = await ItemsService.GetItemById(Guid.Parse(ItemId));

        if(IsAutoBiddingEnabled && UserId != userId)
            CheckAutoBidding();
        
        StateHasChanged();
    }
    private async void RestartTimer(DateTime endedAt, DateTime startedAt)
    {
        var timeTotal = endedAt.Subtract(startedAt).TotalSeconds;
        var timeRest = endedAt.Subtract(DateTime.Now).TotalSeconds;
        
        await ToastService.ShowToast("Auction time was changed", ToastLevel.Info);
        await JsRuntime.InvokeVoidAsync("Timer.RestartTimer", timeTotal,timeRest);
    }
    private async void CheckAutoBidding()
    {
        var res = await ValidateLimit();
        if (res)
        {
            ItemModel.Price += 1;
            SubmitBid();
        }
    }
    private async Task<bool> ValidateLimit()
    {
        var lastBid = BidsList.FirstOrDefault(x => x.Status == BidStatus.InProgress && x.UserId == UserId);
        var value = lastBid?.Bid ?? 0;
        
        var limit = UserSettings.MaximumBid * UserSettings.BidAlertPercent / 100;
        if (ItemModel.Price > UserSettings.AvailableBid + value)
        {
            await ToastService.ShowToast($"You don't have enough money", ToastLevel.Error);
            ItemModel.Price = value;
            IsAutoBiddingEnabled = false;
            return false;
        }

        if (ItemModel.Price == limit)
        {
            await ToastService.ShowToast($"You spent {UserSettings.BidAlertPercent}% of your available balance.", ToastLevel.Warning);
            await Task.Delay(1000);
        }

        return true;
    }

    [JSInvokable]
    public void TimerFinish()
    { 
       NavigationManager.NavigateTo("/");
    }
    private async void SubmitBid()
    {
        if (ItemModel.Price == LootStartPrice)
        {
            await ToastService.ShowToast($"Wait auction end or increase bid", ToastLevel.Info);
            return;
        }
        if (ItemModel.Price < LootStartPrice)
        {
            await ToastService.ShowToast($"Bid must be greater than {LootStartPrice}", ToastLevel.Error);
            return;
        }

        var limitValidated = await ValidateLimit();
        if(!limitValidated)
            return;
        
        LootStartPrice = ItemModel.Price;

        var res = await BidsService.CreateBid(new BidModel()
        {
            Id = Guid.NewGuid(),
            UserId = UserId,
            AuctionId = AuctionModel.Id,
            Bid = ItemModel.Price,
            Date = DateTime.Now,
            Status = BidStatus.InProgress
        });
        if(res.Succeeded)
           await ToastService.ShowToast($"Bid {ItemModel.Price} submitted", ToastLevel.Success);
        else
            await ToastService.ShowToast($"Bid {ItemModel.Price} not submitted", ToastLevel.Error);
    }
    private void OnIsAutoBiddingEnabledChanged(ChangeEventArgs arg)
    {
        if (arg.Value != null)
            IsAutoBiddingEnabled = (bool)arg.Value;
    }
    protected void OrderClick(string propertyName)
    {
        if (propertyName == OrderPropertyName)
            OrderByDesc = !OrderByDesc;
        else
        {
            OrderPropertyName = propertyName;
            OrderByDesc = false;
        }

        GetOrderedPagedItems();
        StateHasChanged();
    }
    private void GetOrderedPagedItems()
    {
        if (OrderByDesc)
            BidsList = BidsList.OrderByDescending(r => r.GetType().GetProperty(OrderPropertyName).GetValue(r)).ToList();
        else
            BidsList = BidsList.OrderBy(r => r.GetType().GetProperty(OrderPropertyName).GetValue(r)).ToList();
    }
    public void Dispose()
    {
        JsRuntime.InvokeVoidAsync("Timer.OnTimesUp", false);
        BidHubServiceUi.NotifyItemChanged -= OnItemSettingsChanged;
        BidHubServiceUi.DisconnectBidHubService();
    }
    private void BackToHome()
    {
        NavigationManager.NavigateTo("/");
    }
}