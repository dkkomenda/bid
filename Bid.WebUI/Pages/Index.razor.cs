﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Bid.Domain.Enums;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Bid.WebUI.Components;
using Bid.WebUI.Hubs;
using Bid.WebUI.Services;
using Bid.WebUI.Services.HttpServices;
using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace Bid.WebUI.Pages
{
    public partial class Index : IDisposable
    {
       [Inject] private NavigationManager NavigationManager { get; set; }
       [Inject] public IModalService Modal { get; set; }
       [Inject] private AuthenticationStateProvider AuthenticationStateProvider { get; set; }
       [Inject] private ItemsHttpService ItemsService { get; set; }
       [Inject] private HubServiceUi HubServiceUi { get; set; }
       [Inject] private UserSettingsHttpService UserSettingsService { get; set; }
       [Inject] private AuctionsHttpService AuctionsService { get; set; }
       [Inject] private ToastService ToastService { get; set; }
       protected List<ItemBase> Items { get; set; } = new();
       protected bool OrderByDesc { get; set; } = false;
       protected string OrderPropertyName { get; set; } = nameof(ItemBase.Description);
       private PagedResultViewModel<ItemBase> ItemsListPaged { get; set; } = new PagedResultViewModel<ItemBase>();
       private bool IsAdmin { get; set; }
       protected override async Task OnInitializedAsync()
       {
           var userRole = await GetUserInfo(ClaimTypes.Role);
           IsAdmin = userRole == "ADMIN";
           HubServiceUi.OnReceivedNewItem += OnReceivedChanges;
           HubServiceUi.OnItemDeleted += OnReceivedChanges;
           await ReloadGrid();
       }
       private async void OnReceivedChanges()
       {
            await ReloadGrid();
       }
       private async Task ReloadGrid()
       {
           Items = (await ItemsService.GetItems())?.ToList() ?? new List<ItemBase>();
           ItemsListPaged = GetCompaniesPaged(Items);
           GetOrderedPagedItems();
           StateHasChanged();
        }
       private void CreateItem()
        {
            var parameters = new ModalParameters();
            var newItem = new ItemModel();

            parameters.Add("Model", newItem);
            Modal.Show<EditItemModal>("Add new item", parameters);
        }
       private void EditItem(ItemBase model)
       {
            var parameters = new ModalParameters();
            var newItem = new ItemModel()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Price = model.Price,
                StartedAt = model.StartedAt,
                EndedAt = model.EndedAt
            };
            
            parameters.Add("Model", newItem);
            Modal.Show<EditItemModal>("Edit item", parameters);
       }
       private void Logout()
       {
           ((CustomAuthStateProvider)AuthenticationStateProvider).LogOut();
       }
       private async Task<string> GetUserInfo(string type)
       {
           var authState = await AuthenticationStateProvider.GetAuthenticationStateAsync();
           var user = authState.User;
           foreach (var claim in user.Claims)
           {
               if (claim.Type == type)
                   return claim.Value;
           }
           return string.Empty;
       }
       private void GetOrderedPagedItems()
       {
           if (OrderByDesc)
               ItemsListPaged.Items = ItemsListPaged.Items.OrderByDescending(r => r.GetType().GetProperty(OrderPropertyName).GetValue(r)).ToList();
           else
               ItemsListPaged.Items = ItemsListPaged.Items.OrderBy(r => r.GetType().GetProperty(OrderPropertyName).GetValue(r)).ToList();
       }
       private PagedResultViewModel<ItemBase> GetCompaniesPaged(IEnumerable<ItemBase> items)
       {
           return new PagedResultViewModel<ItemBase>(items, ItemsListPaged.PageSize);
       }
       protected void SearchChanged(string searchValue)
       {
           if (string.IsNullOrEmpty(searchValue))
           {
               ItemsListPaged = GetCompaniesPaged(Items);
               GetOrderedPagedItems();
               StateHasChanged();
               return;
           }

           var searchedItems = new List<ItemBase>();
           foreach (var item in Items)
           {
               var searchInList = new List<string>();
               switch (OrderPropertyName)
               {
                   case nameof(ItemBase.Name):
                       searchInList = new List<string> { item.Name.ToLower()};
                       break;
                   case nameof(ItemBase.Description):
                       searchInList = new List<string> { item.Description.ToLower()};
                       break;
                   case nameof(ItemBase.Price):
                       searchInList = new List<string> { item.Price.ToString().ToLower()};
                       break;
               }
                   if (searchInList.Any(a => a.IndexOf(searchValue.ToLower()) != -1))
                   searchedItems.Add(item);
           }
           
           ItemsListPaged = GetCompaniesPaged(searchedItems);
           GetOrderedPagedItems();
           StateHasChanged();
       }
       protected void ItemsPageChanged(int page)
       {
           ItemsListPaged.CurrentPage = page;
           StateHasChanged();
       }
       protected void ItemsSizeChanged(int count)
       {
           ItemsListPaged.PageSize = count;
           StateHasChanged();
       }
       protected void OrderClick(string propertyName)
       {
           if (propertyName == OrderPropertyName)
               OrderByDesc = !OrderByDesc;
           else
           {
               OrderPropertyName = propertyName;
               OrderByDesc = false;
           }

           GetOrderedPagedItems();
           StateHasChanged();
       }
       private async Task GoToBid(ItemBase item)
       {
           if (item.EndedAt <= DateTime.Now || item.StartedAt >= DateTime.Now)
           {
               await ToastService.ShowToast("Auction is not available", ToastLevel.Warning);
               return;
           }
           NavigationManager.NavigateTo($"/BidPage/{item.Id.ToString()}");
       }
       public void Dispose()
       {
           HubServiceUi.OnReceivedNewItem -= OnReceivedChanges;
           HubServiceUi.OnItemDeleted -= OnReceivedChanges;
       }
       private async void DeleteItem(Guid itemId)
       {
           await ItemsService.Delete(itemId);
           await AuctionsService.DeleteByItemId(itemId);
       }
       private async void OpenSettingModal()
       {
           var userId = await GetUserInfo(ClaimTypes.NameIdentifier);
           if(string.IsNullOrEmpty(userId))
               return;
           
           var parameters = new ModalParameters();
           parameters.Add("UserId", Guid.Parse(userId));
           Modal.Show<EditSettingsModal>("Edit settings", parameters);
       }
    }
}