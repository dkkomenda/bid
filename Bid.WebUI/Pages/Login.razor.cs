using System;
using System.Threading.Tasks;
using Bid.Domain.ViewModels;
using Bid.WebUI.Services;
using Bid.WebUI.Services.HttpServices;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Newtonsoft.Json;

namespace Bid.WebUI.Pages
{
    public partial class Login 
    {
        [Inject] private AuthorizationService AuthorizationService { get; set; }
        [Inject] private ILocalStorageService LocalStorage { get; set; }
        [Inject] private AuthenticationStateProvider AuthenticationStateProvider { get; set; }
        [Inject] private NavigationManager NavigationManager { get; set; }

        protected EditContext EditContext;
        
        private ValidationMessageStore _messageStore;
        protected LoginViewModel LoginModel { get; set; } = new();
        protected bool IsShowPassword { get; set; }

        protected override void OnInitialized()
        {
            if (!NavigationManager.Uri.Contains("Login") && !NavigationManager.Uri.Contains("ForgotPassword") && !NavigationManager.Uri.Contains("ChangePassword"))
                NavigationManager.NavigateTo("Login");
            EditContext = new EditContext(LoginModel);
            _messageStore = new ValidationMessageStore(EditContext);
            EditContext.OnValidationRequested += (s, e) => _messageStore.Clear();
        }


        protected async Task TryLogin(EditContext editContext)
        {
            var isValid = editContext.Validate();
            if (!isValid)
                return;
            if (!string.IsNullOrEmpty(LoginModel.Email) && !string.IsNullOrEmpty(LoginModel.Password))
            {
                var tokenRes = await AuthorizationService.Login(LoginModel);
                if (tokenRes is null || string.IsNullOrEmpty(tokenRes.AccessToken))
                {
                    _messageStore.Add(editContext.Field("Password"), "Wrong Username or Password");
                    return;
                }
                try
                {
                    await LoadLocalStorageInfo(tokenRes);
                }
                catch (Exception e)
                {
                    await LocalStorage.RemoveItemAsync("_jwt");
                    return;
                    
                }
            
                ((CustomAuthStateProvider)AuthenticationStateProvider).MarkUserAsAuthenticated(tokenRes.Login);
            }
                        
            var authState = await AuthenticationStateProvider.GetAuthenticationStateAsync();
            if (authState.User.Identity is { IsAuthenticated: true })
                NavigationManager.NavigateTo("/", true);
        }

        private async Task LoadLocalStorageInfo(TokenViewModel tokenRes)
        {
            await LocalStorage.SetItemAsync("_jwt", JsonConvert.SerializeObject(tokenRes));
        }
        
        protected void ChangePasswordVisibility()
        {
            IsShowPassword = !IsShowPassword;
        }
    }
}