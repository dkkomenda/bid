﻿using System.Threading.Tasks;
using Bid.WebUI.Hubs;
using Microsoft.AspNetCore.Components;

namespace Bid.WebUI.Shared
{
    public partial class MainLayout
    {
        [Inject] private HubServiceUi HubServiceUi { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await HubServiceUi.ConnectAsync();
                StateHasChanged();
            }
        }
    }
}