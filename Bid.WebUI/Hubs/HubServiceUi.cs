﻿using System;
using System.Threading.Tasks;
using Bid.Domain;
using Bid.Domain.Models;
using Bid.WebUI.Services;
using Microsoft.AspNetCore.Components;
using Bid.Domain.Enums;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;

namespace Bid.WebUI.Hubs
{
    public class HubServiceUi
    {
    
        public HubConnection Connection;

        private readonly ToastService _toastService;

        private string _baseAddress;
        public HubServiceUi(IConfiguration configuration, ToastService toastService)
        {
            _toastService = toastService;
            _baseAddress = configuration["WebApiUrl"];
        }

        #region Actions
        private Action _onReceivedNewItem;
        public event Action OnReceivedNewItem
        {
            add => _onReceivedNewItem += value;
            remove => _onReceivedNewItem -= value;
        }
        
        private Action _onItemDeleted;
        public event Action OnItemDeleted
        {
            add => _onItemDeleted += value;
            remove => _onItemDeleted -= value;
        }
        
        private Action<string,string> _onAuctionEnded;
        public event Action<string,string> OnAuctionEnded
        {
            add => _onAuctionEnded += value;
            remove => _onAuctionEnded -= value;
        }
        #endregion
        public async Task ConnectAsync()
        {
            try
            {
                if (Connection != null && Connection.State == HubConnectionState.Connected)
                    return;
    
                Connection = new HubConnectionBuilder().WithUrl(_baseAddress + "/CommunicationUiHub")
                    .WithAutomaticReconnect()
                    .Build();
    
                Connection.On<ItemBase>(HubEvents.OnReceivedNewItem, async(model) =>
                {
                    _onReceivedNewItem?.Invoke();
                    await _toastService.ShowToast($"{model.Name} was added", ToastLevel.Info);
                });
                Connection.On<ItemBase,Guid>(HubEvents.OnUpdatedItem, async (model, userId) =>
                {
                    _onReceivedNewItem?.Invoke();
                    await _toastService.ShowToast($"{model.Name} was updated", ToastLevel.Info);
                });
                Connection.On<Guid>(HubEvents.OnDeletedItem, (id) =>
                {
                    _onItemDeleted?.Invoke();
                });
                Connection.On<string,string>(HubEvents.OnAuctionEnded, async (modelName, modelPrice) =>
                {
                    _onAuctionEnded?.Invoke(modelName,modelPrice);
                   await _toastService.ShowToast($"Auction for {modelName} ended. Final price - {modelPrice}", ToastLevel.Info);
                });
                await StartConnectionAsync();
    
                Connection.Closed += async (error) => { await StartConnectionAsync(); };
            }
            catch
            {
                return;
            }
        }
        private async Task StartConnectionAsync()
        {
            bool isHubConnect = false;
            while (!isHubConnect)
            {
                try
                {
                    if (Connection.State != HubConnectionState.Connected)
                        await Connection.StartAsync();
                    isHubConnect = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    isHubConnect = false;
                }
    
                Console.WriteLine("SignalR Hub Client Service: isHubConnect {0}", isHubConnect);
            }
        }
    }
}

