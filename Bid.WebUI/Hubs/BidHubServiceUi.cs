﻿using System;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Bid.Domain;
using Bid.Domain.Enums;
using Bid.Domain.Models;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.JSInterop;

namespace Bid.WebUI.Hubs;

public class BidHubServiceUi
{
    private string _baseAddress;

    private HubConnection _connection;
    
    private IJSRuntime _jsRuntime;
    
    private CancellationTokenSource _tokenSource;

    private Action<ItemBase,Guid>_onAuctionTimeChanged;

    public event Action<ItemBase,Guid> NotifyItemChanged
    {
        add => _onAuctionTimeChanged += value;
        remove => _onAuctionTimeChanged -= value;
    }

    public BidHubServiceUi(IConfiguration configuration, IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
        _baseAddress = configuration["WebApiUrl"];
    }
    
    public async Task ConnectAsync(Guid userId)
    {
        try
        {
            if (_connection != null && _connection.State == HubConnectionState.Connected)
                return;
            
            _connection = new HubConnectionBuilder().WithUrl(_baseAddress + "/BidCommunicationUiHub", (options) =>
                {
                    options.Headers["user_id"] = userId.ToString();
                })
                .WithAutomaticReconnect()
                .AddJsonProtocol(jsonOptions => jsonOptions.PayloadSerializerOptions.NumberHandling = JsonNumberHandling.AllowNamedFloatingPointLiterals)
                .Build();
            
            _connection.On<ItemBase,Guid>(HubEvents.OnUpdatedItem, (item,userId) =>
            {
                _onAuctionTimeChanged?.Invoke(item,userId);
            });

            _tokenSource?.Cancel();
            _tokenSource = new();
            await StartConnectionAsync();

            _connection.Closed += StartHubConnectionAsync;
        }
        catch
        {
            return;
        }
    }

    private async  Task StartHubConnectionAsync(Exception arg)
    {
        await StartConnectionAsync();
    }

    private async Task StartConnectionAsync()
    {
        bool isHubConnected = false;
        while (!isHubConnected &&  !_tokenSource.Token.IsCancellationRequested)
        {
            try
            {
                if (_connection.State != HubConnectionState.Connected) 
                    await _connection.StartAsync();
                isHubConnected = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                isHubConnected = false;
                Thread.Sleep(1000);
            }

            Console.WriteLine(@"BidHubUI: isHubConnected {0}", isHubConnected);
        }
    }
    
    public async Task DisconnectBidHubService()
    {
        if(_connection is null)
            return;
        
        _tokenSource?.Cancel();
        _connection.Closed -= StartHubConnectionAsync;
        await this._connection.DisposeAsync();
    }
}