﻿CREATE TABLE [dbo].[UserSettings]
(
[UserId] UNIQUEIDENTIFIER NOT NULL,
[MaximumBid] INTEGER NOT NULL,
[BidAlertPercent] INTEGER NOT NULL,
[AvailableBid] INTEGER NOT NULL
   
CONSTRAINT [FK_UserSettings_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE
)
