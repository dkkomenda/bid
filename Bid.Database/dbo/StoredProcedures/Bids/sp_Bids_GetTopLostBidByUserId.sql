﻿CREATE PROCEDURE [dbo].[sp_Bids_GetTopLostBidByUserId]
    @UserId UNIQUEIDENTIFIER
AS
SELECT TOP(1) *
    FROM [dbo].[Bids]
    WHERE [UserId] = @UserId AND [Status] = 0
RETURN 0