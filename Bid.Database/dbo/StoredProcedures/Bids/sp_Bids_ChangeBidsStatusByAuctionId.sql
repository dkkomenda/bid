﻿CREATE PROCEDURE [dbo].[sp_Bids_ChangeBidsStatusByAuctionId]
    @AuctionId UNIQUEIDENTIFIER
AS
BEGIN
UPDATE [dbo].[Bids]
Set
    [Status] = 0
WHERE [AuctionId] = @AuctionId

UPDATE [dbo].[Bids]
Set
    [Status] = 1
WHERE [AuctionId] = @AuctionId AND [Bid] = (SELECT MAX([BID]) FROM [dbo].[Bids] WHERE [AuctionId] = @AuctionId)
RETURN 0
End