﻿CREATE PROCEDURE [dbo].[sp_Bids_InsertOrUpdate]
    @Id UNIQUEIDENTIFIER,  
    @UserId UNIQUEIDENTIFIER,  
    @AuctionId UNIQUEIDENTIFIER, 
    @Bid INTEGER,
    @Date DATETIME,
    @Status INTEGER
AS
    IF EXISTS(SELECT 1 FROM dbo.Bids WHERE [Id] = @Id)
BEGIN
UPDATE [dbo].[Bids]
Set
    [UserId] = @UserId,
    [AuctionId] = @AuctionId,
    [Bid] = @Bid,
    [Date] = @Date,
    [Status] = @Status
WHERE Id = @Id
END
ELSE
BEGIN
INSERT INTO [dbo].[Bids](
    [Id],
    [UserId],
    [AuctionId],
    [Bid],
    [Date],
    [Status]
)VALUES(
    @Id,
    @UserId,
    @AuctionId,
    @Bid,
    @Date,
    @Status
    )
END
RETURN 0
