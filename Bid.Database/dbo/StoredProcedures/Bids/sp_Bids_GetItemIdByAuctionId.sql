﻿CREATE PROCEDURE [dbo].[sp_Bids_GetItemIdByAuctionId]
@AuctionId UNIQUEIDENTIFIER
AS
	SELECT 
		[ItemId]
	FROM [dbo].[Auctions] WHERE [Id] = @AuctionId
RETURN 0