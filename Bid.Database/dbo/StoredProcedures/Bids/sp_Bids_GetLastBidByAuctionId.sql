﻿CREATE PROCEDURE [dbo].[sp_Bids_GetLastBidByAuctionId]
	@AuctionId UNIQUEIDENTIFIER
AS
SELECT 
    [Id],
    [UserId],
    [AuctionId],
    [Bid],
    [Date],
    [Status]

    FROM [dbo].[Bids] WHERE [AuctionId] = @AuctionId AND [BID] = (SELECT MAX(BID) FROM  [dbo].[Bids] WHERE  [AuctionId] = @AuctionId)

RETURN 0