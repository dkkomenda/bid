﻿CREATE PROCEDURE [dbo].[sp_Bids_GetBidViewModelsByUserId]
	@UserId UNIQUEIDENTIFIER
AS
SELECT 

    I.[Name] AS ItemName,
    B.[Bid],
    B.[Date],
    B.[Status]

    FROM [dbo].[Bids] as B
    INNER JOIN [dbo].[Auctions] AU ON AU.[Id] = B.[AuctionId] 
    INNER JOIN [dbo].[Items] I ON I.[Id] = AU.[ItemId]
    WHERE B.UserId = @UserId 

RETURN 0