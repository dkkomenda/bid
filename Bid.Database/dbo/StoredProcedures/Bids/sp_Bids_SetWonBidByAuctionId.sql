﻿CREATE PROCEDURE [dbo].[sp_Bids_SetWonBidByAuctionId]
    @AuctionId UNIQUEIDENTIFIER
AS
BEGIN
UPDATE [dbo].[Bids]
Set
    [Status] = 2
WHERE [AuctionId] = @AuctionId AND [Bid] = (SELECT MAX([BID]) FROM [dbo].[Bids] WHERE [AuctionId] = @AuctionId)
RETURN 0
End