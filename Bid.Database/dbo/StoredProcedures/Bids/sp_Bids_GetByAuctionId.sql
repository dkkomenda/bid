﻿CREATE PROCEDURE [dbo].[sp_Bids_GetByAuctionId]
	@AuctionId UNIQUEIDENTIFIER
AS
SELECT 
    U.[Id] as UserId,   
    U.[FirstName] AS UserName,
    B.[Bid],
    B.[Date],
    B.[Status]

    FROM [dbo].[Bids] AS B
    INNER JOIN [dbo].Users U ON B.UserId = U.Id
    WHERE B.AuctionId = @AuctionId
RETURN 0