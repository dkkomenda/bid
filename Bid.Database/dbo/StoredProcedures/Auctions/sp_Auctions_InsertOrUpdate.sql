﻿CREATE PROCEDURE [dbo].[sp_Auctions_InsertOrUpdate]
    @Id UNIQUEIDENTIFIER,   
    @ItemId UNIQUEIDENTIFIER, 
    @StartedAt DATETIME,
    @EndedAt DATETIME,
	@IsActive BIT
AS
    IF EXISTS(SELECT 1 FROM dbo.Auctions WHERE [Id] = @Id)
BEGIN
UPDATE [dbo].[Auctions]
Set
    [ItemId] = @ItemId,
    [StartedAt] = @StartedAt,
    [EndedAt] = @EndedAt,
    [IsActive] = @IsActive
WHERE Id = @Id
END
ELSE
BEGIN
INSERT INTO [dbo].[Auctions](
    [Id],
    [ItemId],
    [StartedAt],
    [EndedAt],
    [IsActive]
)VALUES(
    @Id,
    @ItemId,
    @StartedAt,
    @EndedAt,
    @IsActive
    )
END
RETURN 0
