﻿CREATE PROCEDURE [dbo].[sp_Auctions_DeleteByItemId]
	@ItemId UNIQUEIDENTIFIER
AS
	DELETE FROM [dbo].[Auctions] WHERE [ItemId] = @ItemId;	

RETURN 0