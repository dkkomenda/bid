﻿CREATE PROCEDURE [dbo].[sp_Auctions_ChangeAuctionStatus]
    @Id UNIQUEIDENTIFIER,  
    @Status BIT  
AS
BEGIN
UPDATE [dbo].[Auctions]
Set
    [IsActive] = @Status
WHERE Id = @Id
END
RETURN 0
