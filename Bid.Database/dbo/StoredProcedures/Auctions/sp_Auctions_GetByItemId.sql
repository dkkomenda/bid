﻿CREATE PROCEDURE [dbo].[sp_Auctions_GetByItemId]
	@ItemId UNIQUEIDENTIFIER
AS
SELECT 
    [Id],
    [ItemId],
    [StartedAt],
    [EndedAt],
    [IsActive]

    FROM [dbo].[Auctions] WHERE [ItemId] = @ItemId

RETURN 0