﻿CREATE PROCEDURE [dbo].[sp_Auctions_GetWinnerLetter]
    @AuctionId UNIQUEIDENTIFIER,
    @ItemId UNIQUEIDENTIFIER 
AS
SELECT 
    U.[FirstName],
    U.[LastName],
    U.[Email],
    I.[Name] AS ItemName,
    I.[Price] AS ItemPrice,
    A.[EndedAt] AS [Date]

    FROM [dbo].[Auctions] A 
    INNER JOIN [dbo].[Bids] B ON B.AuctionId = @AuctionId
    INNER JOIN [dbo].[Users] U ON B.UserId = U.Id
    INNER JOIN [dbo].[Items] I ON I.Id =  @ItemId
    WHERE A.Id = @AuctionId
RETURN 0