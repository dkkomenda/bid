﻿CREATE PROCEDURE [dbo].[sp_Auctions_GetById]
	@Id UNIQUEIDENTIFIER
AS
SELECT 
    [Id],
    [ItemId],
    [StartedAt],
    [EndedAt],
    [IsActive]

    FROM [dbo].[Auctions] WHERE [Id] = @Id

RETURN 0