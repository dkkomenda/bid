﻿CREATE PROCEDURE [dbo].[sp_Users_GetByNormalizedEmail]
	@NormalizedEmail NVARCHAR(MAX)
AS
	SELECT Users.*, R.Name AS RoleName
    FROM [dbo].[Users] AS Users INNER JOIN [dbo].[Roles] R ON R.Id = Users.[RoleId]
	WHERE Users.[NormalizedEmail] = @NormalizedEmail;
RETURN 0
