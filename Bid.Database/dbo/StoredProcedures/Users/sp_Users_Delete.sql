﻿CREATE PROCEDURE [dbo].[sp_Users_Delete]
	@Id uniqueidentifier
AS
	DELETE FROM [dbo].[Users] WHERE [Id] = @Id;
RETURN 0
