﻿CREATE PROCEDURE [dbo].[sp_Users_GetByNormalizedUserName]
	@NormalizedUserName NVARCHAR(MAX)
AS
	SELECT Users.*, R.Name AS RoleName
	FROM [dbo].[Users] AS Users INNER JOIN [dbo].[Roles] R ON R.Id = Users.[RoleId]
	WHERE [NormalizedUserName] = @NormalizedUserName;
RETURN 0