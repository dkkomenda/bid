﻿CREATE PROCEDURE [dbo].[sp_Users_UpdateRole]
	@Id UNIQUEIDENTIFIER, 
    @RoleId UNIQUEIDENTIFIER,
	@UpdatedAt DATETIME,
	@UpdatedBy UNIQUEIDENTIFIER

AS
	UPDATE [dbo].[Users] SET  
	    [RoleId] = @RoleId,
		[UpdatedAt] = @UpdatedAt,
		[UpdatedBy] = @UpdatedBy
		WHERE ([Id] = @Id);
RETURN 0
