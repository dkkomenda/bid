CREATE PROCEDURE [dbo].[sp_Users_GetUserNameById]
@Id UNIQUEIDENTIFIER
AS
	SELECT
        ([FirstName] + ' ' + [LastName]) as UserName
	FROM [dbo].[Users] WHERE [Id] = @Id;