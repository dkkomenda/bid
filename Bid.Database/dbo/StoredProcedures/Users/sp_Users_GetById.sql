﻿CREATE PROCEDURE [dbo].[sp_Users_GetById]
	@Id uniqueidentifier
AS
	SELECT Users.*, R.Name AS RoleName
    FROM [dbo].[Users] AS Users INNER JOIN [dbo].[Roles] R ON R.Id = Users.[RoleId]
	WHERE Users.[Id] = @Id;
RETURN 0
