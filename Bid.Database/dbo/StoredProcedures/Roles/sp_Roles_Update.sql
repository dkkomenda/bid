﻿CREATE PROCEDURE [dbo].[sp_Roles_Update]
	@Id UNIQUEIDENTIFIER, 
	@Name NVARCHAR(256), 
	@NormalizedName NVARCHAR(256), 
	@ConcurrencyStamp NVARCHAR(MAX), 
	@CreatedAt DATETIME, 
	@UpdatedAt DATETIME
AS
	UPDATE [dbo].[Roles] SET  
		[Name] = @Name,
		[NormalizedName] = @NormalizedName,
		[ConcurrencyStamp] = @ConcurrencyStamp,
		[UpdatedAt] = @UpdatedAt
		WHERE ([Id] = @Id);
RETURN 0
