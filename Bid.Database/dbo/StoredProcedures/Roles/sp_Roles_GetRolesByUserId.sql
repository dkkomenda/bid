﻿CREATE PROCEDURE [dbo].[sp_Roles_GetRolesByUserId]
	@UserId uniqueidentifier
AS
	SELECT [dbo].[Roles].* FROM [dbo].[Roles] 
		INNER JOIN [dbo].[Users] ON	[dbo].[Users].[RoleId] = [dbo].[Roles].[Id]
		WHERE [dbo].[Users].[Id] = @UserId;
RETURN 0
