﻿CREATE PROCEDURE [dbo].[sp_Roles_GetByNormalizedName]
	@NormalizedName nvarchar(256)
AS
	SELECT * FROM [dbo].[Roles] WHERE [NormalizedName] = @NormalizedName;
RETURN 0
