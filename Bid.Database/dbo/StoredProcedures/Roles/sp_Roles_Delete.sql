﻿CREATE PROCEDURE [dbo].[sp_Roles_Delete]
	@Id uniqueidentifier
AS
	DELETE FROM [dbo].[Roles] WHERE [Id] = @Id;
RETURN 0
