﻿CREATE PROCEDURE [dbo].[sp_Roles_GetById]
	@Id uniqueidentifier
AS
	SELECT * FROM [dbo].[Roles] WHERE [Id] = @Id;
RETURN 0
