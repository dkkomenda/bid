﻿CREATE PROCEDURE [dbo].[sp_Roles_Insert]
	@Id UNIQUEIDENTIFIER, 
	@Name NVARCHAR(256), 
	@NormalizedName NVARCHAR(256), 
	@ConcurrencyStamp NVARCHAR(MAX), 
	@CreatedAt DATETIME, 
	@UpdatedAt DATETIME
AS
	INSERT INTO [dbo].[Roles] (
		[Id],
		[Name],
		[NormalizedName],
		[ConcurrencyStamp],
		[CreatedAt],
		[UpdatedAt]
	) VALUES (
		@Id, 
		@Name, 
		@NormalizedName, 
		@ConcurrencyStamp,   
		@CreatedAt, 
		@UpdatedAt
	);
RETURN 0
