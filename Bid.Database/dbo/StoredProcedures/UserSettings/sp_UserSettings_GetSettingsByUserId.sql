﻿CREATE PROCEDURE [dbo].[sp_UserSettings_GetSettingsByUserId]
	@UserId UNIQUEIDENTIFIER
AS
SELECT 
    [UserId],  
    [MaximumBid],
    [BidAlertPercent],
    [AvailableBid]
    FROM [dbo].[UserSettings] WHERE [UserId] = @UserId

RETURN 0