﻿CREATE PROCEDURE [dbo].[sp_UserSettings_InsertOrUpdate]
	@UserId UNIQUEIDENTIFIER,
    @MaximumBid INTEGER,
    @BidAlertPercent INTEGER,
    @AvailableBid INTEGER
AS
    IF EXISTS(SELECT 1 FROM dbo.UserSettings WHERE [UserId] = @UserId)
BEGIN
UPDATE [dbo].[UserSettings]
Set
    [UserId] = @UserId,
    [MaximumBid] = @MaximumBid,
    [BidAlertPercent] = @BidAlertPercent,
    [AvailableBid] = @AvailableBid
WHERE UserId = @UserId
END
ELSE
BEGIN
INSERT INTO [dbo].[UserSettings](
    [UserId],
    [MaximumBid],
    [BidAlertPercent],
    [AvailableBid]
)VALUES(
    @UserId,
    @MaximumBid,
    @BidAlertPercent,
    @AvailableBid
    )
END
RETURN 0
