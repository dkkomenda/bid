﻿CREATE PROCEDURE [dbo].[sp_UserSettings_UpdateAvailableBalanceByNewSettings]
	@UserId UNIQUEIDENTIFIER
AS

	IF NOT EXISTS(SELECT TOP(1) * FROM [dbo].[Bids] WHERE [UserId] = @UserId AND ([Status] = 1 OR [Status]=2))
BEGIN
UPDATE [dbo].[UserSettings]
Set
    [AvailableBid] = [MaximumBid]
WHERE [UserId] = @UserId
END 
	IF EXISTS(SELECT TOP(1) * FROM [dbo].[Bids] WHERE [UserId] = @UserId AND ([Status] = 1 OR [Status]=2))
BEGIN
DECLARE @MaxBidCount INTEGER = (SELECT [MaximumBid] FROM [dbo].[UserSettings] WHERE [UserId] = @UserId)
DECLARE @BidsSum INTEGER = (SELECT SUM([Bid]) FROM [dbo].[Bids] WHERE [UserId] = @UserId AND ([Status] = 1 OR [Status]=2))
UPDATE [dbo].[UserSettings]
Set
    [AvailableBid] = @MaxBidCount - @BidsSum
WHERE [UserId] = @UserId
END

RETURN 0
