﻿CREATE PROCEDURE [dbo].[sp_UserSettings_UpdateAvailableBalanceByUserIdAndAuctionId]
	@UserId UNIQUEIDENTIFIER,
	@AuctionId UNIQUEIDENTIFIER
AS

	IF NOT EXISTS(SELECT TOP(1) * FROM [dbo].[Bids] WHERE [UserId] = @UserId AND [AuctionId]= @AuctionId AND [Status] = 1 OR [Status]=2)
BEGIN
UPDATE [dbo].[UserSettings]
Set
    [AvailableBid] = [MaximumBid]
WHERE [UserId] = @UserId
END 
	IF EXISTS(SELECT TOP(1) * FROM [dbo].[Bids] WHERE [UserId] = @UserId AND [AuctionId]= @AuctionId AND [Status] = 1 OR [Status]=2)
BEGIN
DECLARE @LastValue INTEGER = (SELECT [AvailableBid] FROM [dbo].[UserSettings] WHERE [UserId] = @UserId)
DECLARE @NewBid INTEGER = (SELECT MAX([Bid]) FROM [dbo].[Bids] WHERE [UserId] = @UserId AND [AuctionId]= @AuctionId AND [Status] = 1 OR [Status]=2)
UPDATE [dbo].[UserSettings]
Set
    [AvailableBid] = @LastValue - @NewBid
WHERE [UserId] = @UserId
END

	IF EXISTS(SELECT TOP(1) * FROM [dbo].[Bids] WHERE [UserId] = @UserId AND [AuctionId]= @AuctionId AND [Status] = 0)
BEGIN
DECLARE @Balance INTEGER = (SELECT [AvailableBid] FROM [dbo].[UserSettings] WHERE [UserId] = @UserId)
DECLARE @LostBid INTEGER = (SELECT MAX([Bid]) FROM [dbo].[Bids] WHERE [UserId] = @UserId AND [AuctionId]= @AuctionId AND [Status] = 0)
UPDATE [dbo].[UserSettings]
Set
    [AvailableBid] = @Balance + @LostBid
WHERE [UserId] = @UserId
END

RETURN 0
