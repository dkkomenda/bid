﻿CREATE PROCEDURE [dbo].[sp_UserSettings_UpdateAvailableBalanceByItemId]
	@ItemId UNIQUEIDENTIFIER
AS
DECLARE @AuctionId UNIQUEIDENTIFIER = (SELECT [Id] FROM [dbo].[Auctions] WHERE [ItemId] = @ItemId)
DECLARE @Enumerator TABLE (Id UNIQUEIDENTIFIER)
INSERT INTO @Enumerator
SELECT [Id]
FROM [dbo].[Bids]
WHERE [AuctionId] = @AuctionId AND ([Status] = 1 OR [Status]=2)

DECLARE @BidId UNIQUEIDENTIFIER
	WHILE EXISTS (SELECT 1 from @Enumerator)
	BEGIN
	SELECT TOP 1 @BidId = [Id] from @Enumerator
	
	DECLARE @UserId UNIQUEIDENTIFIER = (SELECT [UserId] FROM [dbo].[Bids] WHERE [Id] = @BidId)
	DECLARE @LastUserBid INTEGER = (SELECT [Bid] FROM [dbo].[Bids] WHERE [Id] = @BidId)
	UPDATE [dbo].[UserSettings]
	Set
		[AvailableBid] = [AvailableBid] + @LastUserBid
	WHERE [UserId] = @UserId

     DELETE FROM @Enumerator where [Id] = @BidId
	END
RETURN 0
