﻿CREATE PROCEDURE [dbo].[sp_Items_GetItemById]
@Id UNIQUEIDENTIFIER
	As 
SELECT 
    [Id],  
    [Name],  
    [Description], 
    [Price], 
	[Image],
    [StartedAt],
    [EndedAt]

    FROM [dbo].[Items] WHERE [Id] = @Id

RETURN 0
