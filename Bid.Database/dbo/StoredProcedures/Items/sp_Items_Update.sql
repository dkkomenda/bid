﻿CREATE PROCEDURE [dbo].[sp_Items_Update]
    @Id UNIQUEIDENTIFIER,  
    @Name NVARCHAR(MAX),  
    @Description NVARCHAR(MAX), 
    @Price INTEGER, 
	@Image NVARCHAR(512),
    @StartedAt DATETIME,
    @EndedAt DATETIME
AS
	UPDATE [dbo].[Items] 
	SET 
        [Name] = @Name, 
        [Description] = @Description, 
        [Price] = @Price, 
	    [Image] = @Image,
        [StartedAt] = @StartedAt,
        [EndedAt] = @EndedAt

	WHERE [Id] = @Id 
RETURN 0
