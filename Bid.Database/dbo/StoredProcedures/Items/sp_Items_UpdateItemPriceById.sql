﻿CREATE PROCEDURE [dbo].[sp_Items_UpdateItemPriceById]
    @ItemId UNIQUEIDENTIFIER,  
    @Price INTEGER
AS
	UPDATE [dbo].[Items] 
	SET 
        [Price] = @Price
	
	WHERE [Id] = @ItemId 
RETURN 0
