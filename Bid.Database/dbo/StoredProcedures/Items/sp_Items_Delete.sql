﻿CREATE PROCEDURE [dbo].[sp_Items_Delete]
	@Id UNIQUEIDENTIFIER

AS
	DELETE FROM [dbo].[Items] WHERE [Id] = @Id;	
	
RETURN 0
