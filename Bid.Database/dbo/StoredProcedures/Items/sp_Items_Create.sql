﻿CREATE PROCEDURE [dbo].[sp_Items_Create]
    @Id UNIQUEIDENTIFIER,  
    @Name NVARCHAR(MAX),  
    @Description NVARCHAR(MAX), 
    @Price INTEGER, 
	@Image NVARCHAR(512),
    @StartedAt DATETIME,
    @EndedAt DATETIME
AS
	INSERT INTO [dbo].[Items] (
    [Id],  
    [Name],  
    [Description], 
    [Price], 
	[Image],
    [StartedAt],
    [EndedAt]
    )VALUES (
    @Id,  
    @Name,  
    @Description, 
    @Price, 
	@Image,
    @StartedAt,
    @EndedAt
    )
    
RETURN 0
