﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The type for column Price in table [dbo].[Items] is currently  DECIMAL (18, 2) NOT NULL but is being changed to  INT NOT
         NULL. Data loss could occur and deployment may fail if the column contains data that is incompatible with type  INT NOT
         NULL.

** User actions
     Alter
       [dbo].[Items] (Table)

** Supporting actions
     Refresh
       [dbo].[sp_Items_Create] (Procedure)
       [dbo].[sp_Items_Delete] (Procedure)
       [dbo].[sp_Items_GetAllItems] (Procedure)
       [dbo].[sp_Items_GetImageById] (Procedure)
       [dbo].[sp_Items_GetItemById] (Procedure)
       [dbo].[sp_Items_Update] (Procedure)

The type for column Price in table [dbo].[Items] is currently  DECIMAL (18, 2) NOT NULL but is being changed to  INT NOT NULL. Data loss could occur and deployment may fail if the column contains data that is incompatible with type  INT NOT NULL.
If this deployment is executed, changes to [dbo].[Items] might introduce run-time errors in [dbo].[sp_Items_Insert].

