﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [dbo].[Items] (Table)
       [dbo].[UserSettings] (Table)
       [dbo].[FK_UserSettings_Users] (Foreign Key)
       [dbo].[FK_Auctions_Items] (Foreign Key)
       [dbo].[FK_Bids_Auctions] (Foreign Key)
       [dbo].[FK_Bids_Users] (Foreign Key)
       [dbo].[sp_UserSettings_UpdateAvailableBidCountByNewSettings] (Procedure)

** Supporting actions
