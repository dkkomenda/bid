﻿/*
Deployment script for Bid

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Bid"
:setvar DefaultFilePrefix "Bid"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Creating Default Constraint unnamed constraint on [dbo].[Auctions]...';


GO
ALTER TABLE [dbo].[Auctions]
    ADD DEFAULT '67CBF896-0B64-4CCE-86F8-D16415CD50F3' FOR [WinnerId];


GO
IF NOT EXISTS (SELECT * FROM [dbo].Roles R WHERE R.[NormalizedName] = N'ADMIN')
BEGIN
INSERT INTO [dbo].Roles VALUES (NEWID(), N'Admin', N'Admin', N'ADMIN', 'ACFA422B-969B-4F8F-9FC2-53A865E93007', GETDATE(), GETDATE())
END 

IF NOT EXISTS (SELECT * FROM [dbo].Roles R WHERE R.[NormalizedName] = N'USER')
BEGIN
INSERT INTO [dbo].Roles VALUES (NEWID(), N'User', N'User', N'USER', 'CAF97D48-9A68-4F4B-B9CF-5DF2580CD264', GETDATE(), GETDATE())
END 

DECLARE @CurentTime DATE = GETDATE()

IF NOT EXISTS (SELECT * FROM [dbo].Users U WHERE U.NormalizedEmail = N'ADMIN@TEST.COM')
BEGIN
	DECLARE @AdminID UNIQUEIDENTIFIER = NEWID()
	INSERT INTO [dbo].Users VALUES (@AdminID, (SELECT R.Id FROM [dbo].Roles as R WHERE R.NormalizedName = N'ADMIN'), N'admin@test.com', N'ADMIN@TEST.COM', 1, N'AQAAAAEAACcQAAAAECxDHHN+kjM1yzD4zvXdO3MPdf8DrWh4Y8nCOapFw+h6+Rc2s1O+PHCVmQD0KGfIdQ==', 0, N'N56KEGPFJBRR3RZ2MZTLHFHUI2O3Y2OD', NULL, 1, 0, N'Admin', N'Admin', N'ADMIN@TEST.COM', N'82ee937c-ed0c-46d0-a723-449b9f2f8e7a', N'+380989898989', 0, 0, 1, NULL, @CurentTime, @AdminID, @CurentTime, @AdminID)
END 

IF NOT EXISTS (SELECT * FROM [dbo].Users U WHERE U.NormalizedEmail = N'USER1@TEST.COM')
BEGIN
	DECLARE @User1ID UNIQUEIDENTIFIER = NEWID()
	INSERT INTO [dbo].Users VALUES (@User1ID, (SELECT R.Id FROM [dbo].Roles as R WHERE R.NormalizedName = N'USER'), N'user1@test.com', N'USER1@TEST.COM', 1, N'AQAAAAEAACcQAAAAEEM11wK/6gb/voZembK9zeFIDKz8p4mfcMgbqKdI8G2fjWm3c0besF6mhnAwXzGk9A==', 0, N'PCDP5G4TF6P3PI4MCNZA4S5XNZYXGZT2', NULL, 1, 0, N'User1', N'User1', N'USER1@TEST.COM', N'9b860a47-d6dc-489e-aba5-069fe9d994fb', N'+380989898991', 0, 0, 1, NULL, @CurentTime, @User1ID, @CurentTime, @User1ID)
END 

IF NOT EXISTS (SELECT * FROM [dbo].Users U WHERE U.NormalizedEmail = N'USER2@TEST.COM')
BEGIN
	DECLARE @User2ID UNIQUEIDENTIFIER = NEWID()
	INSERT INTO [dbo].Users VALUES (@User2ID, (SELECT R.Id FROM [dbo].Roles as R WHERE R.NormalizedName = N'USER'), N'user2@test.com', N'USER2@TEST.COM', 1, N'AQAAAAEAACcQAAAAEGEDal3cXKwQpILCtDgS835MVTKFVfYMOVn1g9QdLCKCrqv6XaPiUeLcil5FCp4gUg==', 0, N'PCDP5G4TF6P3PI4MCNZA4S5XNZYXGZT2', NULL, 1, 0, N'User2', N'User2', N'USER2@TEST.COM', N'9b860a47-d6dc-489e-aba5-069fe9d994fb', N'+380989898992', 0, 0, 1, NULL, @CurentTime, @User2ID, @CurentTime, @User2ID)
END 



IF NOT EXISTS (SELECT * FROM [dbo].UserSettings U WHERE U.UserId = (SELECT Id FROM [dbo].Users WHERE NormalizedEmail = N'ADMIN@TEST.COM'))
BEGIN
INSERT INTO [dbo].UserSettings VALUES ((SELECT Id FROM [dbo].Users WHERE NormalizedEmail = N'ADMIN@TEST.COM'), N'1000', N'90')
END 


IF NOT EXISTS (SELECT * FROM [dbo].UserSettings U WHERE U.UserId = (SELECT Id FROM [dbo].Users WHERE NormalizedEmail = N'USER1@TEST.COM'))
BEGIN
	INSERT INTO [dbo].UserSettings VALUES ((SELECT Id FROM [dbo].Users WHERE NormalizedEmail = N'USER1@TEST.COM'), N'1000', N'90')
END 


IF NOT EXISTS (SELECT * FROM [dbo].UserSettings U WHERE U.UserId = (SELECT Id FROM [dbo].Users WHERE NormalizedEmail = N'USER2@TEST.COM'))
BEGIN
	INSERT INTO [dbo].UserSettings VALUES ((SELECT Id FROM [dbo].Users WHERE NormalizedEmail = N'USER2@TEST.COM'), N'1000', N'90')
END 

GO

GO
PRINT N'Update complete.';


GO
