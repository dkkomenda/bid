﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The column [dbo].[Auctions].[WinnerId] is being dropped, data loss could occur.

** User actions
     Drop
       [dbo].[FK_Auctions_Users] (Foreign Key)
     Alter
       [dbo].[Auctions] (Table)
       [dbo].[sp_Auctions_GetById] (Procedure)
       [dbo].[sp_Auctions_GetByItemId] (Procedure)
       [dbo].[sp_Auctions_GetWinnerLetter] (Procedure)
       [dbo].[sp_Auctions_InsertOrUpdate] (Procedure)
       [dbo].[sp_Bids_ChangeBidsStatusByAuctionId] (Procedure)

** Supporting actions
     Refresh
       [dbo].[sp_Auctions_ChangeAuctionStatus] (Procedure)
       [dbo].[sp_Auctions_DeleteByItemId] (Procedure)
       [dbo].[sp_Auctions_GetActiveAuctions] (Procedure)
       [dbo].[sp_Auctions_GetAuctions] (Procedure)
       [dbo].[sp_Auctions_GetItemIdByAuctionId] (Procedure)
       [dbo].[sp_Bids_GetBidViewModelsByUserId] (Procedure)

The column [dbo].[Auctions].[WinnerId] is being dropped, data loss could occur.
If this deployment is executed, changes to [dbo].[Auctions] might introduce run-time errors in [dbo].[sp_Auctions_UpdateWinnerIdByAuctionId].

