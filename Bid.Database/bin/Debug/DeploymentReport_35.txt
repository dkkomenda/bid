﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [dbo].[Bids] (Table)
       [dbo].[FK_Bids_Users] (Foreign Key)
       [dbo].[FK_Bids_Auctions] (Foreign Key)
       [dbo].[sp_Bids_GetByAuctionId] (Procedure)
       [dbo].[sp_Bids_GetLastBidByAuctionId] (Procedure)
       [dbo].[sp_Bids_InsertOrUpdate] (Procedure)

** Supporting actions
