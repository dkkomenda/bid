# Bid

Prerequsities
dotnet-sdk-6.0.302
Visual Studio 2022(ASP.net .netCore)
Microsoft SQL Server Management Studio

1. Clone repository to your computer.
2. Run Microsoft SQL Server Management Studio, connect to localhost(windows authentication).
3. In Database folder, create new database named Bid.
4. In Visual Studio make Bid.Database publish into the created Bid database.(before publish modify database name from Bid.Database to Bid)
5. Start Bid.WebApi and Bid.WebUI projects.

Credentials:
admin@test.com /admin
admin1@test.com / admin1

user1@test.com / user1
dkkomenda@gmail.com / user2		

You have to add items from admin page by clicking + near search box.
For configuring the auto-bidding and you have to click on cog icon near logout icon.
For changing search parameter click on the column header(name, description, price).
