﻿using Bid.Domain.Models;

namespace Bid.DataAccess.Interfaces;

public interface IUserSettingsDataController
{
    Task<bool> InsertOrUpdate(UserSettingsModel model);
    Task<UserSettingsModel> GetSettingsByUserId(Guid modelId);
    
    //When user changed settings
    Task<bool> UpdateAvailableBalanceByNewSettings(Guid userId);
    
    //When user submitted bid
    Task<bool> UpdateAvailableBalanceByUserIdAndAuctionId(Guid userId, Guid auctionId);
    
    //When item was deleted
    Task<bool> UpdateAvailableBalanceByItemId(Guid itemId);
}