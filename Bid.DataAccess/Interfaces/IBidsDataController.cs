﻿using Bid.Domain.Enums;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;

namespace Bid.DataAccess.Interfaces;

public interface IBidsDataController
{
    Task<bool> InsertOrUpdate(BidModel model);
    Task<IEnumerable<AuctionBidDetailModel>> GetByAuctionId(Guid modelId);
    Task<int> GetLastBidByAuctionId(Guid itemId);
    Task<IEnumerable<BidViewModel>> GetBidViewModelsByUserId(Guid userId);
    Task<bool> ChangeBidsStatusByAuctionId(Guid auctionId);
    Task<Guid> GetItemIdByAuctionId(Guid auctionId);
    Task<bool> SetWonBidByAuctionId(Guid auctionId);
}
