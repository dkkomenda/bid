﻿using Bid.Domain.Models;

namespace Bid.DataAccess.Interfaces;

public interface IItemsDataController
{
    Task<bool> Create(ItemBase model);
    Task<bool> UpdateItem(ItemBase model);
    Task<bool> DeleteItem(Guid itemId);
    Task<IEnumerable<ItemBase>> GetAllItems();
    Task<ItemBase> GetItemById(Guid itemId);
    Task<string> GetImageById(Guid itemId);
    Task<bool> UpdateItemPriceById(Guid itemId, int price);
}