﻿using Bid.Domain.Models;

namespace Bid.DataAccess.Interfaces;

public interface IAuctionsDataController
{
    Task<bool> InsertOrUpdate(AuctionModel model);
    Task<AuctionModel> GetById(Guid modelId);
    Task<bool> DeleteByItemId(Guid itemId);
    Task<AuctionModel> GetByItemId(Guid itemId);
    Task<IEnumerable<AuctionModel>> GetActiveAuctions();
    Task<bool> ChangeAuctionStatus(Guid auctionId, bool status);
    Task<WinnerLetterModel> GetWinnerLetter(AuctionModel auction);
}