﻿using Bid.DataAccess.Interfaces;
using Bid.Domain.Enums;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Bid.DataAccess.DataControllers;

public class BidsDataController: BaseDataController, IBidsDataController
{
    public BidsDataController(IConfiguration configuration) : base(configuration, "Bids")
    { }
    
    public Task<bool> InsertOrUpdate(BidModel model)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", model.Id);
        obj.Add("@UserId", model.UserId);
        obj.Add("@AuctionId", model.AuctionId);
        obj.Add("@Bid", model.Bid);
        obj.Add("@Date", model.Date);
        obj.Add("@Status", model.Status);

        return PerformNonQuery("InsertOrUpdate", obj);
    }
    public Task<IEnumerable<AuctionBidDetailModel>> GetByAuctionId(Guid auctionId)
    {
        var obj = new DynamicParameters();
        obj.Add("@AuctionId", auctionId);
        
        return GetManyAsync<AuctionBidDetailModel>("GetByAuctionId", obj);
    }
    public Task<int> GetLastBidByAuctionId(Guid auctionId)
    {
        var obj = new DynamicParameters();
        obj.Add("@AuctionId", auctionId);
        
        return GetByParamsAsync<int>("GetLastBidByAuctionId", obj);
    }
    public Task<IEnumerable<BidViewModel>> GetBidViewModelsByUserId(Guid userId)
    {
        var obj = new DynamicParameters();
        obj.Add("@UserId", userId);
        
        return GetManyAsync<BidViewModel>("GetBidViewModelsByUserId", obj);
    }

    public Task<bool> ChangeBidsStatusByAuctionId(Guid auctionId)
    {
        var obj = new DynamicParameters();
        obj.Add("@AuctionId", auctionId);

        return PerformNonQuery("ChangeBidsStatusByAuctionId", obj);
    }
    
    public Task<Guid> GetItemIdByAuctionId(Guid auctionId)
    {
        var obj = new DynamicParameters();
        obj.Add("@AuctionId", auctionId);
        
        return GetByParamsAsync<Guid>("GetItemIdByAuctionId", obj);
    }

    public Task<bool> SetWonBidByAuctionId(Guid auctionId)
    {
        var obj = new DynamicParameters();
        obj.Add("@AuctionId", auctionId);

        return PerformNonQuery("SetWonBidByAuctionId", obj);
    }
}