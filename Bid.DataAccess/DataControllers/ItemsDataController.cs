﻿using Bid.DataAccess.Interfaces;
using Bid.Domain.Models;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Bid.DataAccess.DataControllers;

public class ItemsDataController : BaseDataController, IItemsDataController
{
    public ItemsDataController(IConfiguration configuration) 
        : base(configuration, "Items")
    { }

    public Task<bool> Create(ItemBase model)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", model.Id);
        obj.Add("@Name", model.Name);
        obj.Add("@Description", model.Description);
        obj.Add("@Price", model.Price);
        obj.Add("@Image", model.Image);
        obj.Add("@StartedAt", model.StartedAt);
        obj.Add("@EndedAt", model.EndedAt);

        return PerformNonQuery("Create", obj);
    }

    public Task<bool> UpdateItem(ItemBase model)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", model.Id);
        obj.Add("@Name", model.Name);
        obj.Add("@Description", model.Description);
        obj.Add("@Price", model.Price);
        obj.Add("@Image", model.Image);
        obj.Add("@StartedAt", model.StartedAt);
        obj.Add("@EndedAt", model.EndedAt);

        return PerformNonQuery("Update", obj);
    }

    public Task<bool> DeleteItem(Guid itemId)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", itemId);
          
        return PerformNonQuery("Delete", obj);
    }
    public Task<IEnumerable<ItemBase>> GetAllItems()
    {
        return GetManyAsync<ItemBase>("GetAllItems");
    }
    public Task<ItemBase> GetItemById(Guid itemId)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", itemId);
        return GetByParamsAsync<ItemBase>("GetItemById", obj);
    }

    public Task<string> GetImageById(Guid id)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", id);
        return GetByParamsAsync<string>("GetImageById", obj);
    }
    public Task<bool> UpdateItemPriceById(Guid itemId, int price)
    {
        var obj = new DynamicParameters();
        obj.Add("@ItemId", itemId);
        obj.Add("@Price", price);
        return PerformNonQuery("UpdateItemPriceById", obj);
    }
}