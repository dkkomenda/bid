﻿using Bid.DataAccess.Interfaces;
using Bid.Domain.Models;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Bid.DataAccess.DataControllers;

public class UserSettingsDataController: BaseDataController, IUserSettingsDataController
{
    public UserSettingsDataController(IConfiguration configuration) : base(configuration, "UserSettings")
    {
    }
    
    public Task<bool> InsertOrUpdate(UserSettingsModel model)
    {
        var obj = new DynamicParameters();
        obj.Add("@UserId", model.UserId);
        obj.Add("@MaximumBid", model.MaximumBid);
        obj.Add("@BidAlertPercent", model.BidAlertPercent);
        obj.Add("@AvailableBid", model.AvailableBid);

        return PerformNonQuery("InsertOrUpdate", obj);
    }
    public Task<UserSettingsModel> GetSettingsByUserId(Guid userId)
    {        
        var obj = new DynamicParameters();
        obj.Add("@UserId", userId);

        return GetByParamsAsync<UserSettingsModel>("GetSettingsByUserId", obj);
    }
    public Task<bool> UpdateAvailableBalanceByNewSettings(Guid userId)
    {
        var obj = new DynamicParameters();
        obj.Add("@UserId", userId);

        return PerformNonQuery("UpdateAvailableBalanceByNewSettings", obj);
    }
    public Task<bool> UpdateAvailableBalanceByUserIdAndAuctionId(Guid userId, Guid auctionId)
    {
        var obj = new DynamicParameters();
        obj.Add("@UserId", userId);
        obj.Add("@AuctionId", auctionId);

        return PerformNonQuery("UpdateAvailableBalanceByUserIdAndAuctionId", obj);
    }
    public Task<bool> UpdateAvailableBalanceByItemId(Guid itemId)
    {
        var obj = new DynamicParameters();
        obj.Add("@ItemId", itemId);

        return PerformNonQuery("UpdateAvailableBalanceByItemId", obj);
    }
}