﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bid.DataAccess.Interfaces;
using Bid.Domain.Models;
using Microsoft.Extensions.Configuration;

namespace Bid.DataAccess.DataControllers
{
    public class RolesDataController : BaseDataController
    {
        public RolesDataController(IConfiguration configuration) : base(configuration, "Roles")
        {
        }

        public Task<bool> Create(AppIdentityRole role)
        {
            return PerformNonQuery("Insert", role);
        }
        public Task<bool> Update(AppIdentityRole role)
        {
            return PerformNonQuery("Update", role);
        }
        public Task<bool> Delete(Guid id)
        {
            return PerformNonQuery("Delete", new {Id = id});
        }

        public Task<bool> Delete(AppIdentityRole role)
        {
            return Delete(role.Id);
        }
        public Task<AppIdentityRole> GetById(Guid id)
        {
            return GetOneAsync<AppIdentityRole>("GetById", new {Id = id});
        }
        public Task<AppIdentityRole> GetByNormalizedName(string normalizedName)
        {
            return GetOneAsync<AppIdentityRole>("GetByNormalizedName", new {NormalizedName = normalizedName});
        }
        public Task<IEnumerable<AppIdentityRole>> GetAll()
        {
            return GetManyAsync<AppIdentityRole>("GetAll");
        }
        public Task<IEnumerable<AppIdentityRole>> GetByUserId(Guid userId)
        {
            return GetManyAsync<AppIdentityRole>("GetRolesByUserId", new {UserId = userId});
        }
        public async Task<bool> AddUserToRole(AppIdentityUser user, string normalizedRoleName)
        {
            var role = await GetByNormalizedName(normalizedRoleName);

            user.RoleId = role.Id;
            return await PerformNonQuery("sp_Users_UpdateRole", new
            {
                Id = user.Id,
                RoleId = role.Id,
                UpdateedAt = DateTime.UtcNow
            }, true);
        }
    }
}