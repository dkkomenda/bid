﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Bid.DataAccess.DataControllers
{
    public abstract class BaseDataController
    {
        /// <summary>
        /// The configuration
        /// </summary>
        private IConfiguration _configuration;

        /// <summary>
        /// The table name
        /// </summary>
        private string _tableName;

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        protected string ConnectionString => _configuration.GetConnectionString("DefaultConnection");

        /// <summary>
        /// Initializes a new instance of the <see cref="DataController"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="tableName">Name of the table.</param>
        protected BaseDataController(IConfiguration configuration, string tableName)
        {
            _configuration = configuration;
            _tableName = tableName;
        }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <returns></returns>
        protected SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }

        /// <summary>
        /// Prepares the name of the stored procedure.
        /// </summary>
        /// <param name="storedProcedureEnding">The stored procedure ending.</param>
        /// <returns></returns>
        protected string PrepareStoredProcedureName(string storedProcedureEnding)
        {
            return $"sp_{_tableName}_{storedProcedureEnding}";
        }

        /// <summary>
        /// Performs the non query.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="customName">if set to <c>true</c> [custom name].</param>
        /// <returns></returns>
        protected async Task<bool> PerformNonQuery(string storedProcedureName, object parameters, bool customName = false)
        {
            string storedProcedure = customName ? storedProcedureName : PrepareStoredProcedureName(storedProcedureName);
            using (var connection = GetConnection())
            {
                await connection.OpenAsync();
                var rows = await connection.ExecuteAsync(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
                return rows > 0;
            }
        }
        
        /// <summary>
        /// Gets the one asynchronous.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customName">if set to <c>true</c> [custom name].</param>
        /// <returns></returns>
        protected async Task<TModel> GetOneAsync<TModel>(string storedProcedureName, object parameters = null, bool customName = false)
        {
            var storedProcedure = customName ? storedProcedureName : PrepareStoredProcedureName(storedProcedureName);
            
            using (var connection = GetConnection())
            {
                await connection.OpenAsync();
                var row = await connection.QueryFirstOrDefaultAsync<TModel>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
                return row;
            }
        }


        /// <summary>
        /// Gets the many asynchronous.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customName">if set to <c>true</c> [custom name].</param>
        /// <returns></returns>
        protected async Task<IEnumerable<TModel>> GetManyAsync<TModel>(string storedProcedureName, object parameters = null, bool customName = false) where TModel : class
        {
            var storedProcedure = customName ? storedProcedureName : PrepareStoredProcedureName(storedProcedureName);
            using (var connection = GetConnection())
            {
                await connection.OpenAsync();
                
                var rows = await connection.QueryAsync<TModel>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
                return rows;
            }
        }

        /// <summary>
        /// Gets the by parameters asynchronous.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customName">if set to <c>true</c> [custom name].</param>
        /// <returns></returns>
        protected async Task<TModel> GetByParamsAsync<TModel>(string storedProcedureName, DynamicParameters parameters = null, bool customName = false)
        {
            var storedProcedure = customName ? storedProcedureName : PrepareStoredProcedureName(storedProcedureName);
            using (var connection = GetConnection())
            {
                await connection.OpenAsync();
                var row = await connection.QueryFirstOrDefaultAsync<TModel>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
                return row;
            }
        }

        protected async Task<TModel> GetByParamsFromJsonAsync<TModel>(string storedProcedureName, DynamicParameters parameters = null, bool customName = false)
        {
            var storedProcedure = customName ? storedProcedureName : PrepareStoredProcedureName(storedProcedureName);
            using (var connection = GetConnection())
            {
                await connection.OpenAsync();
                var rows = await connection.QueryAsync<string>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
                string tempResult = string.Empty;

                foreach (var item in rows)
                    tempResult = tempResult + item;

                var res = JsonConvert.DeserializeObject<List<TModel>>(tempResult);
                return res != null ? res.FirstOrDefault() : default(TModel);
            }
        }

        protected async Task<IEnumerable<TModel>> GetManyFromJsonAsync<TModel>(string storedProcedureName, DynamicParameters parameters = null, bool customName = false)
        {
            var storedProcedure = customName ? storedProcedureName : PrepareStoredProcedureName(storedProcedureName);
            using (var connection = GetConnection())
            {
                await connection.OpenAsync();
                var rows = await connection.QueryAsync<string>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
                string tempResult = string.Empty;

                foreach (var item in rows)
                    tempResult = tempResult + item;

                var res = JsonConvert.DeserializeObject<IEnumerable<TModel>>(tempResult);
                return res;
            }
        }

    }
}