﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bid.DataAccess.Interfaces;
using Bid.Domain.Models;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Bid.DataAccess.DataControllers
{
    public class UsersDataController: BaseDataController
    {
        public UsersDataController(IConfiguration configuration) : base(configuration, "Users")
        {
        }
        public Task<bool> Create(AppIdentityUser user, Guid? createBy = null)
        {
            user.Id = user.Id == Guid.Empty ? Guid.NewGuid() : user.Id;
            user.CreatedAt = user.UpdatedAt = DateTime.UtcNow;
            createBy ??= Guid.Empty;
            var obj = new DynamicParameters();
            obj.Add($"@{nameof(user.Id)}", user.Id);
            obj.Add($"@{nameof(user.RoleId)}", user.RoleId);
            obj.Add($"@{nameof(user.LockoutEnd)}", user.LockoutEnd);
            obj.Add($"@{nameof(user.TwoFactorEnabled)}", user.TwoFactorEnabled);
            obj.Add($"@{nameof(user.PhoneNumberConfirmed)}", user.PhoneNumberConfirmed);
            obj.Add($"@{nameof(user.PasswordHash)}", user.PasswordHash);
            obj.Add($"@{nameof(user.HasChangedPassword)}", user.HasChangedPassword);
            obj.Add($"@{nameof(user.PhoneNumber)}", user.PhoneNumber);
            obj.Add($"@{nameof(user.ConcurrencyStamp)}", user.ConcurrencyStamp);
            obj.Add($"@{nameof(user.SecurityStamp)}", user.SecurityStamp);
            obj.Add($"@{nameof(user.EmailConfirmed)}", user.EmailConfirmed);
            obj.Add($"@{nameof(user.NormalizedEmail)}", user.NormalizedEmail);
            obj.Add($"@{nameof(user.Email)}", user.Email);
            obj.Add($"@{nameof(user.PasswordHash)}", user.PasswordHash);
            obj.Add($"@{nameof(user.NormalizedUserName)}", user.NormalizedUserName);
            obj.Add($"@{nameof(user.FirstName)}", user.FirstName);
            obj.Add($"@{nameof(user.LastName)}", user.LastName);
            obj.Add($"@{nameof(user.LockoutEnabled)}", user.LockoutEnabled);
            obj.Add($"@{nameof(user.AccessFailedCount)}", user.AccessFailedCount);
            obj.Add($"@{nameof(user.ImageId)}", user.ImageId == Guid.Empty ? null : user.ImageId);
            obj.Add($"@{nameof(user.CreatedAt)}", DateTime.UtcNow);
            obj.Add($"@{nameof(user.UpdatedAt)}", DateTime.UtcNow);
            obj.Add($"@{nameof(user.UpdatedBy)}", user.CreatedBy == Guid.Empty ? createBy : user.CreatedBy);
            obj.Add($"@{nameof(user.CreatedBy)}", user.CreatedBy == Guid.Empty ? createBy : user.CreatedBy);
            return PerformNonQuery("Insert", obj);
        }
        public Task<bool> Update(AppIdentityUser user, Guid? updatedBy = null)
        {
            user.UpdatedAt = DateTime.UtcNow;
            updatedBy ??= Guid.Empty;
            var obj = new DynamicParameters();
            obj.Add($"@{nameof(user.Id)}", user.Id);
            obj.Add($"@{nameof(user.RoleId)}", user.RoleId);
            obj.Add($"@{nameof(user.LockoutEnd)}", user.LockoutEnd);
            obj.Add($"@{nameof(user.TwoFactorEnabled)}", user.TwoFactorEnabled);
            obj.Add($"@{nameof(user.PhoneNumberConfirmed)}", user.PhoneNumberConfirmed);
            obj.Add($"@{nameof(user.PasswordHash)}", user.PasswordHash);
            obj.Add($"@{nameof(user.HasChangedPassword)}", user.HasChangedPassword);
            obj.Add($"@{nameof(user.IsActive)}", user.IsActive);
            obj.Add($"@{nameof(user.PhoneNumber)}", user.PhoneNumber);
            obj.Add($"@{nameof(user.ConcurrencyStamp)}", user.ConcurrencyStamp);
            obj.Add($"@{nameof(user.SecurityStamp)}", user.SecurityStamp);
            obj.Add($"@{nameof(user.EmailConfirmed)}", user.EmailConfirmed);
            obj.Add($"@{nameof(user.NormalizedEmail)}", user.NormalizedEmail);
            obj.Add($"@{nameof(user.Email)}", user.Email);
            obj.Add($"@{nameof(user.NormalizedUserName)}", user.NormalizedUserName);
            obj.Add($"@{nameof(user.FirstName)}", user.FirstName);
            obj.Add($"@{nameof(user.LastName)}", user.LastName);
            obj.Add($"@{nameof(user.LockoutEnabled)}", user.LockoutEnabled);
            obj.Add($"@{nameof(user.AccessFailedCount)}", user.AccessFailedCount);
            obj.Add($"@{nameof(user.ImageId)}", user.ImageId == Guid.Empty ? null : user.ImageId);
            obj.Add($"@{nameof(user.UpdatedAt)}", DateTime.UtcNow);
            obj.Add($"@{nameof(user.UpdatedBy)}", user.UpdatedBy == Guid.Empty ? updatedBy : user.UpdatedBy);
            return PerformNonQuery("Update", obj);
        }
        public Task<bool> Delete(Guid id)
        {
            return PerformNonQuery("Delete", new {Id = id});
        }
        public Task<bool> Delete(AppIdentityUser user)
        {
            return Delete(user.Id);
        }
        public Task<AppIdentityUser> GetById(Guid id)
        {
            return GetOneAsync<AppIdentityUser>("GetById", new {Id = id});
        }
        public Task<IEnumerable<AppIdentityUser>> GetAll()
        {
            return GetManyAsync<AppIdentityUser>("GetAll");
        }
        public Task<AppIdentityUser> GetByNormalizedName(string normalizedName)
        {
            return GetOneAsync<AppIdentityUser>("GetByNormalizedUserName", new {NormalizedUserName = normalizedName});
        }
        public Task<AppIdentityUser> GetByNormalizedEmail(string normalizedEmail)
        {
            var obj = new DynamicParameters();
            obj.Add("NormalizedEmail", normalizedEmail);
            return GetOneAsync<AppIdentityUser>("GetByNormalizedEmail", new {NormalizedEmail = normalizedEmail});
        }
    }
}