﻿using Bid.DataAccess.Interfaces;
using Bid.Domain.Models;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Bid.DataAccess.DataControllers;

public class AuctionsDataController: BaseDataController, IAuctionsDataController
{
    public AuctionsDataController(IConfiguration configuration) : base(configuration, "Auctions")
    { }
    
    public Task<bool> InsertOrUpdate(AuctionModel model)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", model.Id);
        obj.Add("@ItemId", model.ItemId);
        obj.Add("@StartedAt", model.StartedAt);
        obj.Add("@EndedAt", model.EndedAt);
        obj.Add("@IsActive", model.IsActive);

        return PerformNonQuery("InsertOrUpdate", obj);
    }
    public Task<AuctionModel> GetById(Guid modelId)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", modelId);
        
        return GetByParamsAsync<AuctionModel>("GetById", obj);
    }
    public Task<bool> DeleteByItemId(Guid itemId)
    {
        var obj = new DynamicParameters();
        obj.Add("@ItemId", itemId);

        return PerformNonQuery("DeleteByItemId", obj);
    }
    public Task<AuctionModel> GetByItemId(Guid itemId)
    {
        var obj = new DynamicParameters();
        obj.Add("@ItemId", itemId);
        
        return GetByParamsAsync<AuctionModel>("GetByItemId", obj);
    }
    public Task<IEnumerable<AuctionModel>> GetActiveAuctions()
    {
        return GetManyAsync<AuctionModel>("GetActiveAuctions");
    }
    public Task<bool> ChangeAuctionStatus(Guid auctionId, bool status)
    {
        var obj = new DynamicParameters();
        obj.Add("@Id", auctionId);
        obj.Add("@Status", status);

        return PerformNonQuery("ChangeAuctionStatus", obj);
    }

    public Task<WinnerLetterModel> GetWinnerLetter(AuctionModel auction)
    {
        var obj = new DynamicParameters();
        obj.Add("@AuctionId", auction.Id);
        obj.Add("@ItemId", auction.ItemId);
        
        return GetByParamsAsync<WinnerLetterModel>("GetWinnerLetter", obj);
    }
}