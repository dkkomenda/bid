﻿using Bid.DataAccess.DataControllers;
using Bid.DataAccess.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Bid.DataAccess
{
    public static class Startup
    {
        public static void AddDataAccess(this IServiceCollection services)
        {
            services.AddTransient<RolesDataController>();
            services.AddTransient<UsersDataController>();
            services.AddTransient<IItemsDataController,ItemsDataController>();
            services.AddTransient<IUserSettingsDataController,UserSettingsDataController>();
            services.AddTransient<IAuctionsDataController,AuctionsDataController>();
            services.AddTransient<IBidsDataController,BidsDataController>();

        }

        public static void UseDataAccess(this IApplicationBuilder app)
        {
        }

    }
}