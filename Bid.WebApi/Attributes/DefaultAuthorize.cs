using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Bid.WebApi.Attributes
{
    public class DefaultAuthorize : AuthorizeAttribute
    {
        private readonly string[] _allowedRoles;
        public DefaultAuthorize()
        {
            AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme;
        }
        
        public DefaultAuthorize(string allowedRoles)
        {
            _allowedRoles = allowedRoles.Split(",");
            AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme;
        }
        
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var role = context.HttpContext.User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.Role);
            if (!_allowedRoles.Contains(role?.Value.ToUpper()))
                context.Result = new StatusCodeResult((int) System.Net.HttpStatusCode.Forbidden);
        }
    }
}