using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace Bid.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
        protected IHttpContextAccessor HttpContextAccessor { get; }

        protected BaseApiController(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }

        protected IActionResult InternalServerError()
        {
            return StatusCode(500);
        }
        
        protected Guid GetUserId()
        {
            var claims = ((ClaimsIdentity) HttpContextAccessor.HttpContext.User.Identity).Claims;
            var claimValue = claims.FirstOrDefault(f => f.Type == ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrEmpty(claimValue))
                return Guid.Empty;

            return Guid.Parse(claimValue);
        }
        
        protected string GetUserEmail()
        {
            var claims = ((ClaimsIdentity) HttpContextAccessor.HttpContext.User.Identity).Claims;
            return claims.FirstOrDefault(f => f.Type == ClaimTypes.Email)?.Value;
        }
    }
}