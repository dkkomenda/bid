﻿using System.Security.Claims;
using Bid.Business.Identity;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Bid.WebApi.Attributes;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;

namespace Bid.WebApi.Controllers
{
    [ApiController]
    public class AuthController : BaseApiController
    {
        private readonly UserManager<AppIdentityUser> _userManager;
        private readonly SignInManager<AppIdentityUser> _signInManager;
        private readonly IJwtTokenManager _jwtTokenManager;
        private readonly IdentityRoleManager _roleManager;

        public AuthController(UserManager<AppIdentityUser> userManager,
            SignInManager<AppIdentityUser> signInManager,
            IJwtTokenManager jwtTokenManager,
            IHttpContextAccessor contextAccessor,
            IdentityRoleManager roleManager) : base(contextAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtTokenManager = jwtTokenManager;
            _roleManager = roleManager;
        }
        
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !user.IsActive)
                {
                    ModelState.AddModelError(nameof(LoginViewModel.Email), "Not active");
                    return BadRequest(ModelState);
                }

                var isCorrect =
                    await _signInManager.PasswordSignInAsync(user.NormalizedUserName, model.Password, model.RememberMe, false);

                if (!isCorrect.Succeeded)
                {
                    ModelState.AddModelError(nameof(LoginViewModel.Email), "Email or password are incorrect");
                    if (isCorrect.IsLockedOut)
                    {
                        ModelState.AddModelError(nameof(LoginViewModel.Email), "You are locked out");
                    }

                    return BadRequest(ModelState);
                }
                
                var userRole = await _roleManager.GetByNormalizedNameAsync(user.RoleName);
                if(userRole == null)
                    return BadRequest("roleNotFound");

                var token = _jwtTokenManager.GetAuthToken(user);
                return Ok(token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return StatusCode(500);
            }
        }
        
        [HttpPost("ChangePassword")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [DefaultAuthorize]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            try
            {
                var requestResponse = new RequestResponseViewModel();
                var user = await _userManager.FindByEmailAsync(GetUserEmail());
                if (user == null)
                {
                    requestResponse.Errors.Add(new ErrorMessage
                        {Title = "NewPassword", Description = "User not registered"});
                    return BadRequest(requestResponse);
                }

                var result = await _userManager.ChangePasswordAsync(user, model.Password, model.NewPassword);
                if (!result.Succeeded)
                {
                    requestResponse.Errors = result.Errors
                        .Select(s => new ErrorMessage {Title = s.Code, Description = s.Description}).ToList();
                    return BadRequest(requestResponse);
                }

                user.HasChangedPassword = true;
                user.UpdatedBy = GetUserId();

                await _userManager.UpdateAsync(user);

                return NoContent();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return InternalServerError();
            }
        }

        [HttpGet("RefreshAuthToken/{refreshToken}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [DefaultAuthorize]
        public async Task<IActionResult> RefreshAuthToken(string refreshToken)
        {
            try
            {
                var refreshFromToken = GetRefreshToken();
                var expires = GetTokenExpires();
                if (refreshToken != refreshFromToken || expires < DateTime.UtcNow)
                {
                    return BadRequest();
                }

                var userId = GetUserId();

                var user = await _userManager.FindByIdAsync(userId.ToString());
                if (user == null)
                {
                    return BadRequest();
                }
                
                var userRole = await _roleManager.GetByNormalizedNameAsync(user.RoleName);
                if(userRole == null)
                    return BadRequest("roleNotFound");

                var token = _jwtTokenManager.GetAuthToken(user);
                return Ok(token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return InternalServerError();
            }
        }
        
        [HttpGet("GetCurrentUserById/{userId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [DefaultAuthorize]
        public async Task<IActionResult> GetCurrentUserById(Guid userId)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId.ToString());
                if (user == null)
                {
                    return BadRequest();
                }
                
                return Ok(user);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return InternalServerError();
            }
        }
        
        
        [HttpGet("GetCurrentUserId")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [DefaultAuthorize]
        public async Task<IActionResult> GetCurrentUserId()
        {
            try
            {
                var userId = GetUserId();

                var user = await _userManager.FindByIdAsync(userId.ToString());
                if (user == null)
                {
                    return BadRequest();
                }
                
                return Ok(userId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return InternalServerError();
            }
        }
        private string GetRefreshToken()
        {
            var claims = ((ClaimsIdentity) HttpContextAccessor.HttpContext.User.Identity).Claims;
            return claims.FirstOrDefault(f => f.Type == JwtRegisteredClaimNames.Jti)?.Value;
        }

        private DateTime GetTokenExpires()
        {
            var claims = ((ClaimsIdentity) HttpContextAccessor.HttpContext.User.Identity).Claims;
            var claimValue = claims.FirstOrDefault(f => f.Type == ClaimTypes.Expiration)?.Value;

            if (string.IsNullOrEmpty(claimValue))
                return new DateTime();

            return DateTime.Parse(claimValue);
        }
    }
}