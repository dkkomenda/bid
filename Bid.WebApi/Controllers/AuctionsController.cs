﻿using Bid.Business.Interfaces;
using Bid.Domain.Models;
using Bid.WebApi.Attributes;
using Microsoft.AspNetCore.Mvc;

namespace Bid.WebApi.Controllers;

public class AuctionsController : BaseApiController
{
    private readonly IAuctionsService _auctionsService;

    public AuctionsController(IHttpContextAccessor contextAccessor, IAuctionsService auctionsService)
        : base(contextAccessor)
    {
        _auctionsService = auctionsService;
    }
    
    /// <summary>
    /// Create auction.
    /// </summary>
    /// <remarks>Method for creating auction.</remarks>
    /// <returns></returns>
    /// <response code="200">Updates auction.</response>
    /// <response code="400">Does not updates auction.</response>
    [HttpPost("CreateAuction")]
    [ProducesResponseType(200, Type = typeof(bool))]
    [ProducesResponseType(204)]
    [DefaultAuthorize("ADMIN")]
    public async Task<IActionResult> CreateAuction([FromBody] AuctionModel model)
    {
        try
        {
            var res = await _auctionsService.InsertOrUpdate(model);
            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"UpdateBid error {ex}");
            return BadRequest();
        }
    }

    /// <summary>
    /// Get auction model by item Id.
    /// </summary>
    /// <remarks>Method for getting auction model by item Id.</remarks>
    /// <returns></returns>
    /// <response code="200">Get auction model.</response>
    /// <response code="400">Does not get auction model.</response>
    [HttpGet("GetByItemId/{itemId}")]
    [ProducesResponseType(200, Type = typeof(AuctionModel))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> GetByItemId(Guid itemId)
    {
        try
        {
            var res = await _auctionsService.GetByItemId(itemId);
            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"GetAuctionTime error {ex}");
            return BadRequest();
        }
    }

    [HttpDelete("DeleteByItemId/{itemId}")]
    [ProducesResponseType(200)]
    public async Task<IActionResult> DeleteByItemId(Guid itemId)
    {
        try
        {
            var res = await _auctionsService.DeleteByItemId(itemId);
                
            if (!res)
                return BadRequest();

            return Ok();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Delete item error {ex}");
            return BadRequest();
        }
    }
    
    
    [HttpPost("AuctionEnded")]
    [ProducesResponseType(200, Type = typeof(bool))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> AuctionEnded([FromBody] AuctionModel model)
    {
        try
        {
            return Ok();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"UpdateBid error {ex}");
            return BadRequest();
        }
    }
}