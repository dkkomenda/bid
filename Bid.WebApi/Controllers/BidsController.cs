﻿using Bid.Business.Interfaces;
using Bid.Domain.Models;
using Bid.Domain.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Bid.WebApi.Controllers;

public class BidsController : BaseApiController
{
    private readonly IBidsService _bidsService;
    
    public BidsController(IHttpContextAccessor contextAccessor, IBidsService bidsService)
        : base(contextAccessor)
    {
        _bidsService = bidsService;
    }
    
    /// <summary>
    /// Create bid.
    /// </summary>
    /// <remarks>Method for creating bid.</remarks>
    /// <returns></returns>
    /// <response code="200">Create bid.</response>
    /// <response code="400">Does not create bid.</response>
    [HttpPost("CreateBid")]
    [ProducesResponseType(200, Type = typeof(bool))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> CreateBid([FromBody] BidModel model)
    {
        try
        {
            var res = await _bidsService.InsertOrUpdate(model);
            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"CreateBid error {ex}");
            return BadRequest();
        }
    }
    

    [HttpGet("GetBidsByAuctionId/{auctionId}")]
    [ProducesResponseType(200, Type = typeof(IEnumerable<AuctionBidDetailModel>))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> GetBidsByAuctionId(Guid auctionId)
    {
        try
        {
            var res = await _bidsService.GetByAuctionId(auctionId);
            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"GetByAuctionId error {ex}");
            return BadRequest();
        }
    }
    

    [HttpGet("GetLastBidByAuctionId/{auctionId}")]
    [ProducesResponseType(200, Type = typeof(int))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> GetLastBidByAuctionId(Guid auctionId)
    {
        try
        {
            var res = await _bidsService.GetLastBidByAuctionId(auctionId);

            if (res == 0)
                return BadRequest();

            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"GetBidTime error {ex}");
            return BadRequest();
        }
    }
    
    [HttpGet("GetBidViewModelsByUserId/{userId}")]
    [ProducesResponseType(200, Type = typeof(IEnumerable<BidViewModel>))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> GetBidViewModelsByUserId(Guid userId)
    {
        try
        {
            var res = await _bidsService.GetBidViewModelsByUserId(userId);
            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"GetBidTime error {ex}");
            return BadRequest();
        }
    }
}