﻿using Bid.Business.Interfaces;
using Bid.Domain.Models;
using Bid.WebApi.Attributes;
using Microsoft.AspNetCore.Mvc;

namespace Bid.WebApi.Controllers;

public class ItemsController : BaseApiController
{
    private readonly IImageService _imageService;
    
    private readonly IItemsService _itemsService;

    public ItemsController(IHttpContextAccessor contextAccessor, 
        IItemsService itemsService,
        IImageService imageService)
        : base(contextAccessor)
    {
        _itemsService = itemsService;
        _imageService = imageService;
    }
    
    /// <summary>
    /// Create item.
    /// </summary>
    /// <remarks>Method for creating item.</remarks>
    /// <returns></returns>
    /// <response code="200">create item .</response>
    /// <response code="400">does not create item.</response>
    [HttpPost("Create")]
    [ProducesResponseType(200, Type = typeof(bool))]
    [ProducesResponseType(204)]
    [DefaultAuthorize("ADMIN")]
    public async Task<IActionResult> Create([FromBody] ItemModel item)
    {
        try
        {
            var userId = GetUserId();
                if (item == null || userId == Guid.Empty)
                return BadRequest();
            
                
            if (item.Logo != null)
                await _imageService.SaveImage
                    (item.Id, "ItemsLogo", item.Logo, 300, 300);
            

            var res = await _itemsService.Create(item);
                
            if (!res)
                return BadRequest();

            return Ok();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Method for create item {ex}");
            return BadRequest();
        }
    }
    
    /// <summary>
    /// Update item.
    /// </summary>
    /// <remarks>Method for updating item.</remarks>
    /// <returns></returns>
    /// <response code="200">Update item.</response>
    /// <response code="400">Does not update item.</response>
    [HttpPost("Update")]
    [ProducesResponseType(200, Type = typeof(bool))]
    [ProducesResponseType(204)]
    [DefaultAuthorize("ADMIN")]
    public async Task<IActionResult> Update([FromBody] ItemModel item)
    {
        try
        {
            if (item == null)
                return BadRequest();
            
            if (item.Logo != null)
                await _imageService.SaveImage
                    (item.Id, "ItemsLogo", item.Logo, 300, 300);

            var res = await _itemsService.Update(item);
                
            if (!res)
                return BadRequest();

            return Ok();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Method for create item {ex}");
            return BadRequest();
        }
    }
    
    /// <summary>
    /// Get all items.
    /// </summary>
    /// <remarks>Method for getting all existing items.</remarks>
    /// <returns></returns>
    /// <response code="200">Get all items.</response>
    /// <response code="400">Does not get items.</response>
    [HttpGet("GetAllItems")]
    [ProducesResponseType(200, Type = typeof(IEnumerable<ItemBase>))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> GetAllItems()
    {
        try
        {
            var res = await _itemsService.GetAllItems();

            if (res == null)
                return BadRequest();

            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"GetAllItems error {ex}");
            return BadRequest();
        }
    }
    
    /// <summary>
    /// Get item by id
    /// </summary>
    /// <remarks>Method for getting item by id.</remarks>
    /// <returns></returns>
    /// <response code="200">Get item.</response>
    /// <response code="400">Does not get item.</response>
    [HttpGet("GetItemById/{id}")]
    [ProducesResponseType(200)]
    public async Task<IActionResult> GetItemById(Guid id)
    {
        try
        {
            var res = await _itemsService.GetItemById(id);

            if (res == null)
                return BadRequest();

            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"GetItemById error {ex}");
            return BadRequest();
        }
    }
    
    /// <summary>
    /// Get image by item id
    /// </summary>
    /// <remarks>Method for getting image by item id.</remarks>
    /// <returns></returns>
    /// <response code="200">Get item image.</response>
    /// <response code="400">Does not get item image.</response>
    [HttpGet("GetImageById/{id}")]
    [ProducesResponseType(200)]
    public async Task<IActionResult> GetImageById(Guid id)
    {
        try
        {
            var itemImage = await _itemsService.GetImageByItemId(id);

            if (itemImage == null)
                return BadRequest();

            return File(itemImage, "image/png");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"GetImageById error {ex}");
            return BadRequest();
        }
    }
    
    /// <summary>
    /// Delete item by id
    /// </summary>
    /// <remarks>Method for deleting item by id.</remarks>
    /// <returns></returns>
    /// <response code="200">Delete item.</response>
    /// <response code="400">Does not delete item.</response>
    [HttpDelete("Delete/{id}")]
    [ProducesResponseType(200)]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            var res = await _itemsService.Delete(id);
                
            if (!res)
                return BadRequest();

            return Ok();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Delete item error {ex}");
            return BadRequest();
        }
    }
}