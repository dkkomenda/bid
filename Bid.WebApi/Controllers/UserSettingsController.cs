﻿using Bid.Business.Interfaces;
using Bid.Domain.Models;
using Bid.WebApi.Attributes;
using Microsoft.AspNetCore.Mvc;

namespace Bid.WebApi.Controllers;

public class UserSettingsController: BaseApiController
{
    private readonly IUserSettingsService _userSettingsService;

    public UserSettingsController(IHttpContextAccessor contextAccessor, 
        IUserSettingsService userSettingsService)
        : base(contextAccessor)
    {
        _userSettingsService = userSettingsService;
    }
    
    
    /// <summary>
    /// Get user settings by user id
    /// </summary>
    /// <remarks>Method for getting user settings by user id.</remarks>
    /// <returns></returns>
    /// <response code="200">Get user settings.</response>
    /// <response code="400">Does not user settings.</response>
    [HttpGet("GetUserSettingsById/{userId}")]
    [ProducesResponseType(200, Type = typeof(bool))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> GetUserSettingsById(Guid userId)
    {
        try
        {
            var res = await _userSettingsService.GetSettingsByUserId(userId);

            if (res == null)
                return BadRequest();

            return Ok(res);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"GetUserSettingsById error {ex}");
            return BadRequest();
        }
    }
    
    /// <summary>
    /// Update user settings.
    /// </summary>
    /// <remarks>Method for updating user settings.</remarks>
    /// <returns></returns>
    /// <response code="200">Update user settings.</response>
    /// <response code="400">Does not update user settings.</response>
    [HttpPost("Update")]
    [ProducesResponseType(200, Type = typeof(bool))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> Update([FromBody] UserSettingsModel item)
    {
        try
        {
            if (item == null)
                return BadRequest();

            var res = await _userSettingsService.InsertOrUpdate(item);
                
            if (!res)
                return BadRequest();

            return Ok();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Update UserSettingsModel error {ex}");
            return BadRequest();
        }
    }
    
    
    
    /// <summary>
    /// Update and return user balance.
    /// </summary>
    /// <remarks>Method for updating user balance.</remarks>
    /// <returns></returns>
    /// <response code="200">Update user balance.</response>
    /// <response code="400">Does not update user balance.</response>
    [HttpGet("UpdateAvailableBalanceByUserIdAndAuctionId/{userId}/{auctionId}")]
    [ProducesResponseType(200, Type = typeof(bool))]
    [ProducesResponseType(204)]
    public async Task<IActionResult> UpdateAvailableBalanceByUserIdAndAuctionId(Guid userId, Guid auctionId)
    {
        try
        {
            var res = await _userSettingsService.UpdateAvailableBalanceByUserIdAndAuctionId(userId, auctionId);
                
            if (!res)
                return BadRequest();

            return Ok();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Update UserSettingsModel error {ex}");
            return BadRequest();
        }
    }
}