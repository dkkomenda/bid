﻿namespace Bid.Domain.Models;

public class AuctionModel
{
    public Guid Id { get; set; }
    public Guid ItemId { get; set; }
    public DateTime StartedAt { get; set; }
    public DateTime EndedAt { get; set; }
    public bool IsActive { get; set; }
}