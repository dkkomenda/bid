﻿using System.ComponentModel.DataAnnotations;

namespace Bid.Domain.Models;

public class ItemBase
{
    public Guid Id { get; set; }
    
    [Required(ErrorMessageResourceName = "NameIsRequired", ErrorMessageResourceType = typeof(ModelResources.ErrorMessageResource))]
    public string Name { get; set; }

    [Required(ErrorMessageResourceName = "DescriptionIsRequired", ErrorMessageResourceType = typeof(ModelResources.ErrorMessageResource))]
    public string Description { get; set; }
    
    [Required(ErrorMessageResourceName = "PriceIsRequired", ErrorMessageResourceType = typeof(ModelResources.ErrorMessageResource))]
    [Range(1, Int32.MaxValue)]
    public int Price { get; set; }
    public string Image { get; set; }
    public DateTime StartedAt { get; set; }
    public DateTime EndedAt { get; set; }
}