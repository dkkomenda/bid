﻿namespace Bid.Domain.Models;

public class WinnerLetterModel
{
    public string FirstName { get; set; }
    
    public string LastName { get; set; }

    public string Email { get; set; }

    public string ItemName { get; set; }
    
    public string ItemPrice { get; set; }
    
    public DateTime Date { get; set; }
}