﻿using System.ComponentModel.DataAnnotations;

namespace Bid.Domain.Models;

public class UserSettingsModel
{
    public Guid UserId { get; set; }
    
    [Range(1, Int32.MaxValue)]
    public int MaximumBid { get; set; }
    
    [Range(1, 100, ErrorMessageResourceName = "BidAlertPercentRange",ErrorMessageResourceType = typeof(ModelResources.ErrorMessageResource))]
    public int BidAlertPercent { get; set; }
    public int AvailableBid { get; set; }
}