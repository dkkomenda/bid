﻿using Bid.Domain.Enums;

namespace Bid.Domain.Models;

public class BidModel
{
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public Guid AuctionId { get; set; }
    public int Bid { get; set; }
    public DateTime Date { get; set; }
    public BidStatus Status { get; set; }
}