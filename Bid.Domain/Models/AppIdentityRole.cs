using System;
using Microsoft.AspNetCore.Identity;

namespace Bid.Domain.Models
{
    public class AppIdentityRole: IdentityRole<Guid>
    {		
        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
        
        public string Description { get; set; }
    }
}