using System;

namespace Bid.Domain.Models
{
    public class UserBase
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public string UserName => $"{FirstName} {LastName}";
        
        public string PhoneNumber { get; set; }
        
        public string Email { get; set; }
        
        public Guid CreatedBy { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public Guid UpdatedBy { get; set; }
        
        public DateTime UpdatedAt { get; set; }
        
        public bool IsActive { get; set; }
        
        public byte[] Photo { get; set; }
        
        public string Role { get; set; }
        
        public string RoleDescription { get; set; }
        
    }
}