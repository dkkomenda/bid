using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;

namespace Bid.Domain.Models
{
    public class AppIdentityUser: IdentityUser<Guid>
    {
        public AppIdentityUser()
        {
            Claims = new HashSet<Claim>();
        }
        public AppIdentityUser(string userName) : base(userName)
        {
            Claims = new HashSet<Claim>();
        }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public Guid RoleId { get; set; }
        
        public string RoleName { get; set; }
        
        public HashSet<Claim> Claims { get; set; }
        
        public bool HasChangedPassword { get; set; } 
        
        public bool IsActive { get; set; }
        
        public Guid? ImageId { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public Guid CreatedBy { get; set; }
        
        public DateTime UpdatedAt { get; set; }
        
        public Guid UpdatedBy { get; set; }
    }
}