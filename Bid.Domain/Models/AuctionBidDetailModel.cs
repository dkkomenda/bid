﻿using Bid.Domain.Enums;

namespace Bid.Domain.Models;

public class AuctionBidDetailModel
{
    public Guid UserId { get; set; }
    public string UserName { get; set; }
    public int Bid { get; set; }
    public DateTime Date { get; set; }
    public BidStatus Status { get; set; }
}