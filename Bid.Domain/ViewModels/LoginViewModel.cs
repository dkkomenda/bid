using System.ComponentModel.DataAnnotations;

namespace Bid.Domain.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceName = "EmailIsRequired", ErrorMessageResourceType = typeof(ModelResources.ErrorMessageResource))]
        [Display(Name = "Email")]
        [RegularExpression(@"^[\d\w\._\-]+@([\d\w\._\-]+\.)+[\w]+$", ErrorMessageResourceName="EmailIsInvalid", ErrorMessageResourceType=typeof(ModelResources.ErrorMessageResource))]
        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required(ErrorMessageResourceName = "PasswordIsRequired", ErrorMessageResourceType = typeof(ModelResources.ErrorMessageResource))]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}   