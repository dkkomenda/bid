﻿
namespace Bid.Domain.ViewModels
{
    public abstract class PagedResultBase
    {
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int MaxVisiblePageCount { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
    }
    
    //for ui paging
    public class PagedResultViewModel<T> : PagedResultBase where T: class
    {   
        public List<T> Items { get; set; }

        public PagedResultViewModel()
        {
            CurrentPage = 1;
            PageSize = 10;
            Items = new List<T>();
            TotalItems = 0;
            PageCount = 1;
        }

        public PagedResultViewModel(IEnumerable<T> items, int pageSize, int maxVisiblePageCount = 1)
        {
            CurrentPage = 1;
            PageSize = pageSize;
            Items = items == null ? new List<T>() : items.ToList();
            TotalItems = Items.Count();
            PageCount = (int)Math.Ceiling((decimal)TotalItems / PageSize);
            MaxVisiblePageCount = maxVisiblePageCount;
        }

        public List<T> GetPagedResult()
        {
            return Items.Count() == 0 ? Items : Items.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();
        }
        
        public int GetPageByItem(T item)
        {
            var itemIndex = Items.FindIndex(i => i == item);
            return (int) itemIndex / PageSize + 1;
        }

    }
}
