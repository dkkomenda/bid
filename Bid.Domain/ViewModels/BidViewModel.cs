﻿using Bid.Domain.Enums;

namespace Bid.Domain.ViewModels;

public class BidViewModel
{
    public string ItemName { get; set; }

    public int Bid { get; set; }
    
    public DateTime Date { get; set; }

    public BidStatus Status { get; set; }
}