﻿namespace Bid.Domain.Enums;

public enum ResponseMessageProviderEnum
{
    Success = 0,
    ValidationError = 1,
    ProviderError = 2
}