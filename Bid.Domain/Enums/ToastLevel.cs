﻿namespace Bid.Domain.Enums;

public enum ToastLevel
{
    Info,
    Success,
    Warning,
    Error
}