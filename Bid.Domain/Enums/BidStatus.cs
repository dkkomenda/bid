﻿namespace Bid.Domain.Enums;

public enum BidStatus
{
    Lost = 0,
    InProgress = 1,
    Won = 2
}