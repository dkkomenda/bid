﻿using Microsoft.Extensions.Configuration;

namespace Bid.Domain.Helpers
{
    public static class ConfigurationHelper
    {
        public static T Get<T>(this IConfiguration configuration, string sectionName)
        {
            return configuration.GetSection(sectionName).Get<T>();
        }
    } 
}


