﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Bid.Domain.Helpers
{
    public static class ImageHelper
    {
        public static Stream ToStream(this Image image, ImageFormat format)
        {
            var stream = new MemoryStream();
            image.Save(stream, format);
            stream.Position = 0;
            return stream;
        }

        public static void ImageFromBase64(string base64Img)
        {
            var bytes = Convert.FromBase64String(base64Img);
            using (var img = new FileStream(@"result.svg", FileMode.Create))
            {
                img.Write(bytes, 0, bytes.Length);
                img.Flush();    
            }
        }

        public static byte[] ResizeImage(byte[] data, int width, int height)
        {
            try
            {
                using (var ms = new MemoryStream(data))
                using (var image = Image.FromStream(ms))
                    return ResizeImage(image, width, height);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        
        public static byte[] ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            try
            {
                destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            }
            catch (Exception e)
            {
                // ignored
            }

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            using (var memoryStream = new MemoryStream())
            {
                destImage.Save(memoryStream, ImageFormat.Png);
                return memoryStream.ToArray();
            }
        }
        public static byte[] ImageToByteArray(this Image imageIn)
        {
            var ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }
    }
}
