﻿namespace Bid.Domain;

public static class HubEvents
{
    public const string OnReceivedNewItem = "OnReceivedNewItem";
    
    public const string OnUpdatedItem = "OnUpdatedItem";
    
    public const string OnPriceChanged = "OnPriceChanged";
    
    public const string OnDeletedItem = "OnDeletedItem";
    
    public const string OnAuctionEnded = "OnAuctionEnded";
}
